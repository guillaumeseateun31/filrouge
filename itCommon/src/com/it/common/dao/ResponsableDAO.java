package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Responsable;

public class ResponsableDAO extends AbstractDAO<Responsable> implements IResponsableDAO {

	
	public ResponsableDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Responsable> findAll() {
		List<Responsable> Responsables = null;

		Responsables = em.createQuery("from Responsable").getResultList();

		return Responsables;
	}
	
	@Override
	public Responsable save(Responsable responsable) {

		return super.save(responsable);
	}


	@Override
	public Responsable get(Integer id) {
		Responsable Responsable = null;
		Responsable = em.find(Responsable.class, id);
		return Responsable;
	}


}

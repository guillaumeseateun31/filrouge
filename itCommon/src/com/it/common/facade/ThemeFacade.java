package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Session;
import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;
import com.it.common.dao.IThemeDAO;

public class ThemeFacade implements IThemeFacade {

	private IThemeDAO themeDAO = null;

	public ThemeFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Theme> findAll() {
		List<Theme> themes = null;

		// appel à la couche DAO
		themes = themeDAO.findAll();

		return themes;
	}

	@Override
	public Theme get(Integer id) {
		Theme theme = themeDAO.get(id);
		return theme;
	}

	@Override
	public Theme save(Theme theme) {
		Theme themeSave = themeDAO.save(theme);
		return themeSave;
	}

	@Override
	public Theme update(Theme theme) {
		Theme themeUpdate = themeDAO.update(theme);
		return themeUpdate;
	}

	@Override
	public void delete(Theme theme) {
		themeDAO.delete(theme);
		
	}

	public IThemeDAO getThemeDAO() {
		return themeDAO;
	}

	public void setThemeDAO(IThemeDAO themeDAO) {
		this.themeDAO = themeDAO;
	}

	@Override
	public List<Soustheme> getSousTheme(Integer id) {
		List<Soustheme> soustheme = null;

		// appel à la couche DAO
		soustheme = themeDAO.getSousTheme(id);

		return soustheme;
	}
	



}

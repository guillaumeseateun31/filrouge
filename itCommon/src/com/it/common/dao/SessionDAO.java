package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Session;

public class SessionDAO extends AbstractDAO<Session> implements ISessionDAO {

	
	public SessionDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Session> findAll() {
		List<Session> sessions = null;

		sessions = em.createQuery("from Session").getResultList();

		return sessions;
	}


	@Override
	public Session get(Integer id) {
		Session session = null;
		session = em.find(Session.class, id);
		return session;
	}

	@Override
	public List<Session> getAllByDomaine(Integer id_domaine) {
		List<Session> sessions = null;

		sessions = em.createQuery("SELECT s from Session s, Domaine d, Theme t, Decompositiontheme dt, Soustheme st, Programmeformation pf, Formation f WHERE d.id = t.domaine.id AND t.id = dt.theme.id AND dt.soustheme.id = st.id AND st.id = pf.soustheme.id AND pf.formation.id = f.id AND f.id = s.formation.id AND d.id='" +id_domaine + "'").getResultList();

		return sessions;
		
		/*
		 * TypedQuery<Account> tp = em.createQuery("SELECT a FROM Account a, User u   WHERE u.account_id = a.id AND a.email = :email AND a.pwd = :pwd AND a.role = 'admin'", Account.class);
            tp.setParameter("email", this.username);
            tp.setParameter("pwd", this.password);
            Account result = tp.getSingleResult();
		 */
	}



}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Soustheme implements Serializable {

	private static final long serialVersionUID = 8356434288035431432L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nom;
	private float coutHT;
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinTable(name = "decomposition_theme", joinColumns = @JoinColumn(name = "id_soustheme", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_theme", referencedColumnName = "id", updatable = true))
	private List<Theme> themes = new ArrayList<Theme>();
	


	public Soustheme() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Soustheme(Integer id, String nom, float coutHT) {
		super();
		this.id = id;
		this.nom = nom;
		this.coutHT = coutHT;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public float getCoutHT() {
		return coutHT;
	}
	public void setCoutHT(float coutHT) {
		this.coutHT = coutHT;
	}
	
	public List<Theme> getThemes() {
		return themes;
	}

	public void setThemes(List<Theme> themes) {
		this.themes = themes;
	}
	

}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Formateur implements Serializable {

	private static final long serialVersionUID = 6391012224122342173L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private float prix;
	private boolean isNouveau;
	private boolean isExclu;

	@OneToMany( targetEntity=Session.class, mappedBy="formateur" )
    private List<Session> sessions = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "competence_formateur", joinColumns = @JoinColumn(name = "id_formateur", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_competence", referencedColumnName = "id", updatable = true))
	private List<Soustheme> sousthemes = new ArrayList<Soustheme>();
	@ManyToOne @JoinColumn(name="id_utilisateur", nullable=false)
    private Utilisateur utilisateur;
	
	public Formateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Formateur(Integer id, float prix, boolean isNouveau, boolean isExclu) {
		super();
		this.id = id;
		this.prix = prix;
		this.isNouveau = isNouveau;
		this.isExclu = isExclu;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public boolean isNouveau() {
		return isNouveau;
	}

	public void setNouveau(boolean isNouveau) {
		this.isNouveau = isNouveau;
	}

	public boolean isExclu() {
		return isExclu;
	}

	public void setExclu(boolean isExclu) {
		this.isExclu = isExclu;
	}
	
	

}

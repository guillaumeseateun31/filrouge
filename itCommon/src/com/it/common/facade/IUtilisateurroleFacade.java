package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Utilisateurrole;

public interface IUtilisateurroleFacade {

	List<Utilisateurrole> findAll();

	Utilisateurrole save(Utilisateurrole utilisateurrole);

	Utilisateurrole get(Integer id);
	
	Utilisateurrole getByEmail(String email);

	Utilisateurrole update(Utilisateurrole utilisateurrole);

	void delete(Utilisateurrole utilisateurrole);

}

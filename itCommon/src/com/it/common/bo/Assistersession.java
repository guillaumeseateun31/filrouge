package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@Table(name = "assister_session")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Assistersession implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1455136853429515267L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@ManyToOne @JoinColumn(name="id_session", nullable=false)
    private Session session;

	@ManyToOne @JoinColumn(name="id_stagiaire", nullable=false)
    private Stagiaire stagiaire;
	
	@ManyToOne @JoinColumn(name="id_evaluation", nullable=false)
    private Evaluation evaluation;


	public Assistersession() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Assistersession(Integer id, Session session, Stagiaire stagiaire, Evaluation evaluation) {
		super();
		this.id = id;
		this.session = session;
		this.stagiaire = stagiaire;
		this.evaluation = evaluation;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Session getSession() {
		return session;
	}


	public void setSession(Session session) {
		this.session = session;
	}


	public Stagiaire getStagiaire() {
		return stagiaire;
	}


	public void setStagiaire(Stagiaire stagiaire) {
		this.stagiaire = stagiaire;
	}


	public Evaluation getEvaluation() {
		return evaluation;
	}


	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}
	
	

	
	

}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Formation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8431319183191980140L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nom;
	private String reference;
	private String descriptif;
	private Integer nbrMinStagiaire;
	private Integer nbrMaxStagiaire;
	private String lieu;
	private boolean isIntra;
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinTable(name = "programme_formation", joinColumns = @JoinColumn(name = "id_formation", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_soustheme", referencedColumnName = "id", updatable = true))
	private List<Soustheme> sousthemes = new ArrayList<Soustheme>();
	/*
	@OneToMany( targetEntity=Session.class, mappedBy="formation" )
    private List<Session> sessions = new ArrayList<>();
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "assister_session", joinColumns = @JoinColumn(name = "id_formation", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_evaluation", referencedColumnName = "id", updatable = true))
	private List<Evaluation> evaluations = new ArrayList<Evaluation>();
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "assister_session", joinColumns = @JoinColumn(name = "id_formation", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_stagiaire", referencedColumnName = "id", updatable = true))
	private List<Stagiaire> stagiaires = new ArrayList<Stagiaire>();
	
	*/
	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Formation(Integer id, String nom, String reference, String descriptif, Integer nbrMinStagiaire,
			Integer nbrMaxStagiaire, String lieu, boolean isIntra) {
		super();
		this.id = id;
		this.nom = nom;
		this.reference = reference;
		this.descriptif = descriptif;
		this.nbrMinStagiaire = nbrMinStagiaire;
		this.nbrMaxStagiaire = nbrMaxStagiaire;
		this.lieu = lieu;
		this.isIntra = isIntra;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDescriptif() {
		return descriptif;
	}
	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}
	public Integer getNbrMinStagiaire() {
		return nbrMinStagiaire;
	}
	public void setNbrMinStagiaire(Integer nbrMinStagiaire) {
		this.nbrMinStagiaire = nbrMinStagiaire;
	}
	public Integer getNbrMaxStagiaire() {
		return nbrMaxStagiaire;
	}
	public void setNbrMaxStagiaire(Integer nbrMaxStagiaire) {
		this.nbrMaxStagiaire = nbrMaxStagiaire;
	}
	public String getLieu() {
		return lieu;
	}
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	public boolean isIntra() {
		return isIntra;
	}
	public void setIntra(boolean isIntra) {
		this.isIntra = isIntra;
	}

	
	public List<Soustheme> getSousthemes() {
		return sousthemes;
	}

	public void setSousthemes(List<Soustheme> sousthemes) {
		this.sousthemes = sousthemes;
	}
/*
	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}

	public List<Evaluation> getEvaluations() {
		return evaluations;
	}

	public void setEvaluations(List<Evaluation> evaluations) {
		this.evaluations = evaluations;
	}

	public List<Stagiaire> getStagiaires() {
		return stagiaires;
	}

	public void setStagiaires(List<Stagiaire> stagiaires) {
		this.stagiaires = stagiaires;
	}
*/
	@Override
	public String toString() {
		return "Formation [id=" + id + ", nom=" + nom + ", reference=" + reference + ", descriptif=" + descriptif
				+ ", nbrMinStagiaire=" + nbrMinStagiaire + ", nbrMaxStagiaire=" + nbrMaxStagiaire + ", lieu=" + lieu
				+ ", isIntra=" + isIntra + ", sousthemes=" + sousthemes + "]";
	}


}

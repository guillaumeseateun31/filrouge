package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Evaluation;
import com.it.common.dao.IEvaluationDAO;

public class EvaluationFacade implements IEvaluationFacade {

	private IEvaluationDAO evaluationDAO = null;

	public EvaluationFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Evaluation> findAll() {
		List<Evaluation> evaluations = null;

		// appel à la couche DAO
		evaluations = evaluationDAO.findAll();

		return evaluations;
	}

	public Evaluation save(Evaluation evaluation) {
		Evaluation evaluationSave = evaluationDAO.save(evaluation);
		return evaluationSave;
	}

	/**
	 * @return the evaluationDAO
	 */
	public IEvaluationDAO getEvaluationDAO() {
		return evaluationDAO;
	}

	/**
	 * @param evaluationDAO the evaluationDAO to set
	 */
	public void setEvaluationDAO(IEvaluationDAO evaluationDAO) {
		this.evaluationDAO = evaluationDAO;
	}

	@Override
	public Evaluation get(Integer id) {
		Evaluation evaluation = evaluationDAO.get(id);
		return evaluation;
	}

	@Override
	public Evaluation update(Evaluation evaluation) {
		Evaluation evaluationUpdate = evaluationDAO.update(evaluation);
		return evaluationUpdate;
	}

	@Override
	public void delete(Evaluation evaluation) {
		evaluationDAO.delete(evaluation);
	}

}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Typeresponsable implements Serializable {

	private static final long serialVersionUID = 6372571668006624992L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String libelle;
	@OneToMany( targetEntity=Responsable.class, mappedBy="typeresponsable" )
    private List<Stagiaire> stagiaires = new ArrayList<>();
	
	public Typeresponsable() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Typeresponsable(Integer id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	

}

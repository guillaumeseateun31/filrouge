package com.it.comon.test.facade;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.it.common.bo.Domaine;
import com.it.common.bo.Evaluation;
import com.it.common.bo.Session;
import com.it.common.bo.Stagiaire;
import com.it.common.bo.Utilisateur;
import com.it.common.bo.Assistersession;
import com.it.common.facade.AssistersessionFacade;
import com.it.common.facade.EvaluationFacade;
import com.it.common.facade.IAssistersessionFacade;
import com.it.common.facade.IEvaluationFacade;
import com.it.common.facade.ISessionFacade;
import com.it.common.facade.IStagiaireFacade;
import com.it.common.facade.SessionFacade;
import com.it.common.facade.StagiaireFacade;

class AssistersessionFacadeTest {
	private static IAssistersessionFacade assistersessionFacade = null;
	private static ISessionFacade sessionFacade = null;
	private static IStagiaireFacade stagiaireFacade = null;
	private static IEvaluationFacade evaluationFacade = null;
	
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// demarrage de spring
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextCommon.xml");

		assistersessionFacade = context.getBean("assistersessionFacade", AssistersessionFacade.class);
		stagiaireFacade = context.getBean("stagiaireFacade", StagiaireFacade.class);
		sessionFacade = context.getBean("sessionFacade", SessionFacade.class);
	}
	
	
	@Test
	void testFindAll() {
		List<Assistersession> assistersessions = assistersessionFacade.findAll();

		// on controle le bouchon
		assertNotNull(assistersessions);
		//assertEquals(2, assistersessions.size());
	}
	
	@Test
	void testSave() {
		Utilisateur utilisateur = new Utilisateur(1, "test", "test", null, null, null, null, null, null, null, null);
		Stagiaire stagiaire = new Stagiaire(1, true, utilisateur);
		Evaluation evaluation = new Evaluation(1, null, null, null, null, null, null);
		Session session = new Session(1, null, null, null);
		Assistersession assistersession = new Assistersession(1, session, stagiaire, evaluation);
		Assistersession assistersessionTest = assistersessionFacade.save(assistersession);

		// on controle le bouchon
		assertNotNull(assistersessionTest);
		//assertEquals(2, assistersessions.size());
	}


}

/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Formation;

/**
 * @author it
 *
 */
public interface IFormationFacade {

	List<Formation> findAll();

	Formation get(Integer id);

	Formation save(Formation formation);

	Formation update(Formation formation);

	void delete(Formation formation);

}

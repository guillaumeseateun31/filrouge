-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 20 mai 2021 à 08:58
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibliodb`
--

--
-- Déchargement des données de la table `livre`
--

INSERT INTO `livre` (`id`, `dateDeParution`, `isbn`, `titre`) VALUES
(3, '2021-05-18 00:00:00', 'SQDSD-59595', '50 nuances de bleu et rose'),
(2, '2021-05-25 00:00:00', 'mlmbllb-55445', '1 nuance de vert caca d\'oie'),
(4, '2021-06-03 00:00:00', 'aze', 'qsd'),
(65, '2021-05-19 00:00:00', 'mlmbllb-55445', '1 nuance de vert caca d\'oie'),
(54, NULL, 'rze', 'dd'),
(55, '2021-05-19 00:00:00', 'ddd', 'dd'),
(61, '2021-05-27 00:00:00', 'rze', 'qqq'),
(66, '2021-05-25 00:00:00', 'mlmbllb-55445', '1 nuance de vert caca d\'oie modif'),
(67, '2021-05-27 00:00:00', 'rze', 'qqq MODIF'),
(68, '2021-05-27 00:00:00', 'rze', 'qqq MODIF 2'),
(69, '2021-05-27 00:00:00', 'rze', 'qqq MODIF 3'),
(70, '2021-05-27 00:00:00', 'rze', 'qqq MODIF 5'),
(71, '2021-05-18 00:00:00', 'SQDSD-59595', '50 nuances de bleu'),
(72, '2021-05-18 00:00:00', 'SQDSD-59595', '50 nuances de bleu'),
(73, '2021-05-18 00:00:00', 'SQDSD-59595', '50 nuances de bleu');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

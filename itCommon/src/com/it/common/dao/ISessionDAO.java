package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Session;

public interface ISessionDAO extends IDAO<Session> {
	List<Session> getAllByDomaine(Integer id);
}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Formateur;

public interface IFormateurFacade {

	List<Formateur> findAll();

	Formateur save(Formateur formateur);

	Formateur get(Integer id);

	Formateur update(Formateur formateur);

	void delete(Formateur formateur);

}

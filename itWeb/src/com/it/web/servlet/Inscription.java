package com.it.web.servlet;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


import com.it.common.bo.Utilisateur;
import com.it.common.bo.Utilisateurrole;
import com.it.common.facade.IUtilisateurFacade;
import com.it.common.facade.IUtilisateurroleFacade;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/inscription" })
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUtilisateurFacade utilisateurFacade;
	private IUtilisateurroleFacade utilisateurroleFacade;

	/**
	 * Default constructor.
	 */
	public Inscription() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/inscription.jsp");

		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String erreur = "";
		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/inscriptionOk.jsp");

		// recuperation du nom
		String nom = request.getParameter("nom");
		// recuperation du prenom
		String prenom = request.getParameter("prenom");
		// recuperation de l'email
		String email = request.getParameter("email");
		// recuperation du prenom
		String password = request.getParameter("password");

		// controle des champs du formulaire
		if (nom == null || nom.isEmpty()) {
			// champ nom obligatoire
			erreur += "champ nom obligatoire<br />";

		}
		if (prenom == null || prenom.isEmpty()) {
			// champ nom obligatoire
			erreur += "champ prenom obligatoire<br />";
		}
		
		if (email == null || email.isEmpty()) {
			// champ nom obligatoire
			erreur += "champ email obligatoire<br />";
		}


		// creation de l utilisateur
		Utilisateur utilisateur = new Utilisateur(null, nom, prenom, email, password, null, null, null, null, null, null);

		if (!erreur.isEmpty()) {
			// on place l erreur dans la request
			request.setAttribute("erreur", erreur);
			// on a une erreur
			requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
		} else {
			// sauvegarde de l'utlisateur
			utilisateurFacade.save(utilisateur);
			Utilisateurrole utilisateurrole = new Utilisateurrole(null, utilisateur.getEmail(), "utilisateur");
			utilisateurroleFacade.save(utilisateurrole);
			
		}

		// utilisateurFacade.save(utilisateur);
		// passage des infos dans la session
		request.getSession().setAttribute("utilisateur", utilisateur);
		/*
		 * request.getSession().setAttribute("nom", nom);
		 * request.getSession().setAttribute("prenom", prenom);
		 */
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);
	}

	/**
	 * @return the utilisateurFacade
	 */
	public IUtilisateurFacade getUtilisateurFacade() {
		return utilisateurFacade;
	}

	/**
	 * @param utilisateurFacade the utilisateurFacade to set
	 */
	@Autowired
	public void setUtilisateurFacade(IUtilisateurFacade utilisateurFacade) {
		this.utilisateurFacade = utilisateurFacade;
	}

	public IUtilisateurroleFacade getUtilisateurroleFacade() {
		return utilisateurroleFacade;
	}

	@Autowired
	public void setUtilisateurroleFacade(IUtilisateurroleFacade utilisateurroleFacade) {
		this.utilisateurroleFacade = utilisateurroleFacade;
	}

	
}

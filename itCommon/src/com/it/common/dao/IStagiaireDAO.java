package com.it.common.dao;

import com.it.common.bo.Stagiaire;

public interface IStagiaireDAO extends IDAO<Stagiaire> {

}

package com.it.rs.soustheme;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.it.common.facade.ISousthemeFacade;
import com.it.common.bo.Soustheme;

@Component
@Path("/soustheme")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class SousthemeService {

	private ISousthemeFacade sousthemeFacade = null;

	public SousthemeService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("{id}")
	public Soustheme getSoustheme(@PathParam("id") Integer id) {
		Soustheme soustheme = sousthemeFacade.get(id);
		return soustheme;
	}

	@GET
	@Path("/sousthemes")
	public List<Soustheme> findAll() {
		List<Soustheme> sousthemes = sousthemeFacade.findAll();
		return sousthemes;
	}

	@DELETE
	@Path("{id}")
	public String delete(@PathParam("id") Integer id) {
		Soustheme soustheme = new Soustheme(id, null, 0);
		sousthemeFacade.delete(soustheme);
		return "ok";
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public String ajouter(Soustheme soustheme) {
		sousthemeFacade.save(soustheme);
		return "ok";
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	public String update(Soustheme soustheme) {
		sousthemeFacade.update(soustheme);
		return "ok";
	}

	public ISousthemeFacade getSousthemeFacade() {
		return sousthemeFacade;
	}

	public void setSousthemeFacade(ISousthemeFacade sousthemeFacade) {
		this.sousthemeFacade = sousthemeFacade;
	}



}
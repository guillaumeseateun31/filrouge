package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Session;
import com.it.common.bo.Utilisateurrole;

public interface IUtilisateurroleDAO extends IDAO<Utilisateurrole> {
	Utilisateurrole getByEmail(String email);
}

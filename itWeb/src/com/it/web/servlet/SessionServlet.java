package com.it.web.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.support.SecurityContextProvider;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.it.common.bo.Assistersession;
import com.it.common.bo.Utilisateur;
import com.it.common.facade.IAssistersessionFacade;
import com.it.common.facade.IUtilisateurFacade;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/sessions" })
public class SessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUtilisateurFacade utilisateurFacade;
	private IAssistersessionFacade assistersessionFacade;

	/**
	 * Default constructor.
	 */
	public SessionServlet() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/sessions.jsp");
		HttpSession session = request.getSession();
		
		if(session.getAttribute("user") == null)
		{
			String test = request.getUserPrincipal().getName();
			Utilisateur user = utilisateurFacade.getByEmail(request.getUserPrincipal().getName());
			request.getSession().setAttribute("user", user);
			session.setAttribute("user", user);
		}

		Utilisateur utilisateur = utilisateurFacade.getByEmail(request.getUserPrincipal().getName());
		List<Assistersession> assistersessions = assistersessionFacade.getByUser(utilisateur.getId());
		//List<Assistersession> assistersessions = assistersessionFacade.findAll();
		request.setAttribute("assistersessions", assistersessions);
		
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);

	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/signature.jsp");

		// recuperation du nom
		String signature = request.getParameter("id_evaluation");
		
		if(signature != null)
		{
			// passage des livres dans la requete pour la view
			request.setAttribute("signature", signature);
		}

		 
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);
	}
	/**
	 * @return the utilisateurFacade
	 */
	public IUtilisateurFacade getUtilisateurFacade() {
		return utilisateurFacade;
	}

	/**
	 * @param utilisateurFacade the utilisateurFacade to set
	 */
	@Autowired
	public void setUtilisateurFacade(IUtilisateurFacade utilisateurFacade) {
		this.utilisateurFacade = utilisateurFacade;
	}

	public IAssistersessionFacade getAssistersessionFacade() {
		return assistersessionFacade;
	}

	@Autowired
	public void setAssistersessionFacade(IAssistersessionFacade assistersessionFacade) {
		this.assistersessionFacade = assistersessionFacade;
	}
	
	

}

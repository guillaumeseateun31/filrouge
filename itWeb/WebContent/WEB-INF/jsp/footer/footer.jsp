
    <footer class="ftco-footer ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">IT Training</h2>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, error repellat vitae inventore a possimus dolore quas similique impedit magnam, fugiat nisi quibusdam sequi earum nihil voluptates dolores blanditiis hic!</p>
              <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4 ml-md-4">
              <h2 class="ftco-heading-2">Certifications</h2>
              <ul class="list-unstyled">
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Home</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>About</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Services</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Projects</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Contact</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Partenaires</h2>
              <ul class="list-unstyled">
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Architectural Design</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Interior Design</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Exterior Design</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Lighting Design</a></li>
                <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>AutoCAD Service</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart color-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank"></a>TeamMascotte</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Connexion</h4>
        </div>
        <div class="d-flex flex-column text-center">
          <form action="j_security_check" method="post" id="formLoginHeader">
            <div class="form-group">
              <input type="email" name="j_username" class="form-control" id="email1" placeholder="Votre adresse mail ...">
            </div>
            <div class="form-group">
              <input type="password" name="j_password" class="form-control" id="password1" placeholder="Votre mot de passe ...">
            </div>
            <button type="submit" type="button" class="btn btn-success btn-block btn-round">Se connecter</button>
          </form>

      </div>
    </div>
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section">Pas encore inscrit ? <button id="connecttologin" class="btn btn-primary"  data-dismiss="modal"> S'inscrire </button></div>
      </div>
  </div>
</div>
</div>


<div class="modal fade" id="inscriptionModal" tabindex="-1" role="dialog" aria-labelledby="dd" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Inscription</h4>
        </div>
        <div class="d-flex flex-column text-center">
          <form action="inscription" method="post" id="inscriptionForm">
          <p class="erreur"><c:out value="${requestScope.erreur }" escapeXml="false"></c:out> </p>
            <div class="form-group">
              <input type="email" name="email" value="${sessionScope.utilisateur.email }"  class="form-control" id="email2" placeholder="Adresse mail">
            </div>
            <div class="form-group">
              <input type="text" name="nom" value="${sessionScope.utilisateur.email }" class="form-control" id="nom" placeholder="Nom">
            </div>
            <div class="form-group">
              <input type="text" name="prenom" value="${sessionScope.utilisateur.prenom }" class="form-control" id="prenom" placeholder="Pr�nom">
            </div>
            <div class="form-group">
              <input type="password" name="password" class="form-control" id="password2" placeholder="Mot de passe">
            </div>
            <input type="submit"  class="btn btn-success btn-block btn-round">S'inscrire</input>

          </form>

      </div>
    </div>

  </div>
</div>
</div>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>



  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/bootstrap-select.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script>
  $(document).ready(function() {             

	  $('#login').on('click', function () {
		  $('#loginModal').modal('toggle');
		});

	  $('#connecttologin').on('click', function () {
	      /*  $('#loginModal').modal('hide');
	        $('#inscriptionModal').modal('show');*/
	        $('#loginModal').modal('hide');
	        $('#inscriptionModal').modal('show');
	        
		});

	  $('#logintoconnect').on('click', function () {
		  $('#loginModal').modal('toggle');
		  $('#inscriptionModal').modal('hide');
		});

	});
  </script>
  <script src="js/main.js"></script>
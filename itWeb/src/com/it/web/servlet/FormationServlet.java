package com.it.web.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.it.common.facade.IDomaineFacade;
import com.it.common.facade.IFormationFacade;
import com.it.common.facade.ISousthemeFacade;
import com.it.common.facade.IThemeFacade;
import com.it.common.bo.Domaine;
import com.it.common.bo.Formation;
import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;

@WebServlet({ "/formations" })
public class FormationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private IFormationFacade formationFacade;
	private IDomaineFacade domaineFacade;
	private IThemeFacade themeFacade;
	private ISousthemeFacade sousthemeFacade;
	/**
	 * Default constructor.
	 */
	public FormationServlet() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/formations.jsp");
		
		List<Formation> formations = formationFacade.findAll();
		List<Domaine> domaines = domaineFacade.findAll();
		List<Theme> themes = themeFacade.findAll();
		//List<Soustheme> sousthemes = sousthemeFacade.findAll();
		
		// passage des livres dans la requete pour la view
		request.setAttribute("formations", formations);
		request.setAttribute("domaines", domaines);
		request.setAttribute("themes", themes);
		//request.setAttribute("sousthemeFacade", sousthemeFacade);

		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);


	}

	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	/**
	 * @return the livreAC
	 */
	public IFormationFacade getFormationFacade() {
		return formationFacade;
	}

	/**
	 * @param livreAC the livreAC to set
	 */
	@Autowired
	public void setFormationFacade(IFormationFacade formationFacade) {
		this.formationFacade = formationFacade;
	}
	
	public IDomaineFacade getDomaineFacade() {
		return domaineFacade;
	}

	@Autowired
	public void setDomaineFacade(IDomaineFacade domaineFacade) {
		this.domaineFacade = domaineFacade;
	}

	public IThemeFacade getThemeFacade() {
		return themeFacade;
	}
	
	@Autowired
	public void setThemeFacade(IThemeFacade themeFacade) {
		this.themeFacade = themeFacade;
	}
	
	public ISousthemeFacade getSousthemeFacade() {
		return sousthemeFacade;
	}
	
	@Autowired
	public void setSousthemeFacade(ISousthemeFacade sousthemeFacade) {
		this.sousthemeFacade = sousthemeFacade;
	}
	


}

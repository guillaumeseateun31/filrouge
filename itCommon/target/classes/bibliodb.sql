-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 20 mai 2021 à 09:22
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bibliodb`
--

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `nom`) VALUES(1, 'admin');
INSERT INTO `role` (`id`, `nom`) VALUES(2, 'utilisateur');
INSERT INTO `role` (`id`, `nom`) VALUES(3, 'utilisateurBis');

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `login`, `nom`, `password`, `prenom`) VALUES(1, 'biblio', 'iblio', 'password', 'biblio');
INSERT INTO `utilisateur` (`id`, `login`, `nom`, `password`, `prenom`) VALUES(6, 'test', 'testupdate', 'test', 'testupdate');
INSERT INTO `utilisateur` (`id`, `login`, `nom`, `password`, `prenom`) VALUES(5, 'ttest3', 'test3', 'test3', 'test3');
INSERT INTO `utilisateur` (`id`, `login`, `nom`, `password`, `prenom`) VALUES(8, 'test4', 'test4', 'test4', 'test4');

--
-- Déchargement des données de la table `utilisateur_role`
--

INSERT INTO `utilisateur_role` (`login`, `nom`) VALUES('biblio', 'admin');
INSERT INTO `utilisateur_role` (`login`, `nom`) VALUES('biblio', 'utilisateur');
INSERT INTO `utilisateur_role` (`login`, `nom`) VALUES('test', 'utilisateur');
INSERT INTO `utilisateur_role` (`login`, `nom`) VALUES('test4', 'utilisateur');
INSERT INTO `utilisateur_role` (`login`, `nom`) VALUES('ttest3', 'utilisateur');

-- Déchargement des données de la table `livre`
INSERT INTO livre (id, dateDeParution, isbn, titre) VALUES(1, '2021-04-01 00:00:00', 'isbn 1', '50 nuances de chat');
INSERT INTO livre (id, dateDeParution, isbn, titre) VALUES(2, '2021-04-01 00:00:00', 'isbn 2', 'le seigneur des POEC');
INSERT INTO livre (id, dateDeParution, isbn, titre) VALUES(3, '2021-04-01 00:00:00', 'isbn 3', 'Harry Port du Havre');

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

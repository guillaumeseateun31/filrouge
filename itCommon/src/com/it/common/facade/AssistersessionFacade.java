package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Assistersession;
import com.it.common.dao.IAssistersessionDAO;

public class AssistersessionFacade implements IAssistersessionFacade {

	private IAssistersessionDAO assistersessionDAO = null;

	public AssistersessionFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Assistersession> findAll() {
		List<Assistersession> assistersessions = null;

		// appel à la couche DAO
		assistersessions = assistersessionDAO.findAll();

		return assistersessions;
	}

	@Override
	public Assistersession get(Integer id) {
		Assistersession assistersession = assistersessionDAO.get(id);
		return assistersession;
	}

	@Override
	public Assistersession save(Assistersession assistersession) {
		Assistersession assistersessionSave = assistersessionDAO.save(assistersession);
		return assistersessionSave;
	}

	@Override
	public Assistersession update(Assistersession assistersession) {
		Assistersession assistersessionUpdate = assistersessionDAO.update(assistersession);
		return assistersessionUpdate;
	}

	@Override
	public void delete(Assistersession assistersession) {
		assistersessionDAO.delete(assistersession);
		
	}

	public IAssistersessionDAO getAssistersessionDAO() {
		return assistersessionDAO;
	}

	public void setAssistersessionDAO(IAssistersessionDAO assistersessionDAO) {
		this.assistersessionDAO = assistersessionDAO;
	}

	public List<Assistersession> getByUser(Integer id) {
		List<Assistersession> assistersessions = assistersessionDAO.getByUser(id);
		return assistersessions;
	}


}

package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Utilisateur;

public class UtilisateurDAO extends AbstractDAO<Utilisateur> implements IUtilisateurDAO {

	
	public UtilisateurDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Utilisateur> findAll() {
		List<Utilisateur> utilisateurs = null;

		utilisateurs = em.createQuery("from Utilisateur").getResultList();

		return utilisateurs;
	}

	public Utilisateur save(Utilisateur utilisateur) {

		return super.save(utilisateur);
	}

	@Override
	public Utilisateur get(Integer id) {
		Utilisateur utilisateur = null;
		utilisateur = em.find(Utilisateur.class, id);
		return utilisateur;
	}
	
	@Override
	public Utilisateur getByEmail(String email) {
		Utilisateur utilisateur = null;
		utilisateur = (Utilisateur) em.createQuery("from Utilisateur WHERE email = '" +email + "'").getSingleResult();
		return utilisateur;
	}


}

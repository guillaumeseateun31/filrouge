package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Soustheme;
import com.it.common.dao.ISousthemeDAO;

public class SousthemeFacade implements ISousthemeFacade {

	private ISousthemeDAO sousthemeDAO = null;

	public SousthemeFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Soustheme> findAll() {
		List<Soustheme> sousthemes = null;

		// appel à la couche DAO
		sousthemes = sousthemeDAO.findAll();

		return sousthemes;
	}

	@Override
	public Soustheme get(Integer id) {
		Soustheme soustheme = sousthemeDAO.get(id);
		return soustheme;
	}

	@Override
	public Soustheme save(Soustheme soustheme) {
		Soustheme sousthemeSave = sousthemeDAO.save(soustheme);
		return sousthemeSave;
	}

	@Override
	public Soustheme update(Soustheme soustheme) {
		Soustheme sousthemeUpdate = sousthemeDAO.update(soustheme);
		return sousthemeUpdate;
	}

	@Override
	public void delete(Soustheme soustheme) {
		sousthemeDAO.delete(soustheme);
		
	}

	public ISousthemeDAO getSousthemeDAO() {
		return sousthemeDAO;
	}

	public void setSousthemeDAO(ISousthemeDAO sousthemeDAO) {
		this.sousthemeDAO = sousthemeDAO;
	}



}

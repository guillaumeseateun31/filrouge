/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Session;

/**
 * @author it
 *
 */
public interface ISessionFacade {

	List<Session> findAll();

	Session get(Integer id);
	
	List<Session> getSessionByDomaine(Integer id_domaine);

	Session save(Session session);

	Session update(Session session);
	
	void delete(Session session);

}

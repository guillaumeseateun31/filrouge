package com.it.comon.test.facade;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.it.common.bo.Domaine;
import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;
import com.it.common.facade.ThemeFacade;
import com.it.common.facade.IThemeFacade;

class ThemeFacadeTest {
	private static IThemeFacade themeFacade = null;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// demarrage de spring
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextCommon.xml");

		themeFacade = context.getBean("themeFacade", ThemeFacade.class);
	}
	
	
	@Test
	void testFindAll() {
		List<Theme> themes = themeFacade.findAll();

		// on controle le bouchon
		assertNotNull(themes);
		//assertEquals(2, themes.size());
	}
	
	@Test
	void testGetSoustheme() {
		List<Soustheme> themes = themeFacade.getSousTheme(1);

		// on controle le bouchon
		assertNotNull(themes);
		//assertEquals(2, domaines.size());
	}

}

<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>

<title>Sessions</title>
<jsp:include page="head/head.jsp"></jsp:include>

</head>
<body onload="initPage()">
	<div id="page">
		<jsp:include page="header/header.jsp"></jsp:include>
		<jsp:include page="nav/nav.jsp"></jsp:include>
		<div id="content" style="margin-top:5em;margin-bottom:5em;">


<div class="row">
<div class="container-fluid p-0">
						<div class="col-12 col-lg-12 col-xxl-12 d-flex">
							<div class="card flex-fill">
								<div class="card-header">

									<h5 class="card-title mb-0">Sessions inscrits</h5>
								</div>
								<table class="table table-hover my-0">
									<thead>
										<tr>
											<th>Nom</th>
											<th class="d-none d-xl-table-cell">Date début</th>
											<th class="d-none d-xl-table-cell">Date fin</th>
											<th>Status</th>
											<th class="d-none d-md-table-cell">Signature</th>
										</tr>
									</thead>
									<tbody>
									<c:forEach items="${assistersessions }" var="assistersession">
									<tr>
											<td>${assistersession.session.nom }</td>
											<td class="d-none d-xl-table-cell"><fmt:formatDate pattern = "dd/MM/yyyy"  value = "${assistersession.session.dateDebut }" /></td>
											<td class="d-none d-xl-table-cell"><fmt:formatDate pattern = "dd/MM/yyyy"  value = "${assistersession.session.dateFin }" /> </td>
											
											<c:if test="${assistersession.evaluation.signature == 'signé' }">
											<td><span class="badge bg-success">fini</span></td>
											
											 
											 <td><span class="badge bg-success">Signé</span></td>
											 </c:if>
											 <c:if test="${assistersession.evaluation.signature != 'signé' }">
											 <td><span class="badge bg-warning">en cours</span></td>
											 <td class="d-none d-xl-table-cell"><button type="button" value="${assistersession.evaluation.id }" class="signature btn btn-danger">A SIGNER</a></td>
											 
											 </c:if>
											
										</tr>
									</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
</div>
					</div>



<form action="sessions" method="post" id="signatureform">
 	<input type="hidden" name="id_evaluation"  id="id_evaluation" value="">

  </form>  
		</div>
		<jsp:include page="footer/footer.jsp"></jsp:include>
				
				
	</div>
			<script>
$( document ).ready(function() {
   $(".active").removeClass("active");
   $("#sessions").addClass("active");

   $(".signature").on("click", function () {
		var id_evaluation = this.value;
		$("#id_evaluation").val(id_evaluation);
		$("#signatureform").submit();
   });
   
});
</script>
</body>
</html>
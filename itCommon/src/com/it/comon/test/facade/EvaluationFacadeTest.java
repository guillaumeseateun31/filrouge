package com.it.comon.test.facade;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.it.common.bo.Domaine;
import com.it.common.bo.Evaluation;
import com.it.common.facade.EvaluationFacade;
import com.it.common.facade.IEvaluationFacade;

class EvaluationFacadeTest {
	private static IEvaluationFacade evaluationFacade = null;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// demarrage de spring
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextCommon.xml");

		evaluationFacade = context.getBean("evaluationFacade", EvaluationFacade.class);
	}
	
	
	@Test
	void testFindAll() {
		List<Evaluation> evaluations = evaluationFacade.findAll();

		// on controle le bouchon
		assertNotNull(evaluations);
		//assertEquals(2, evaluations.size());
	}
	
	@Test
	void testSave() {
		Evaluation evaluation = new Evaluation(null, null, null, null, null, null, null);
		Evaluation evaluationTest = evaluationFacade.save(evaluation);

		// on controle le bouchon
		assertNotNull(evaluationTest);
		//assertEquals(2, evaluations.size());
	}
	

}

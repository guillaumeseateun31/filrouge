package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Typeresponsable;

public interface ITyperesponsableFacade {

	List<Typeresponsable> findAll();

	Typeresponsable save(Typeresponsable typeresponsable);

	Typeresponsable get(Integer id);

	Typeresponsable update(Typeresponsable typeresponsable);

	void delete(Typeresponsable typeresponsable);

}

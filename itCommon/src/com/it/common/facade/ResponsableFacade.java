package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Responsable;
import com.it.common.dao.IResponsableDAO;

public class ResponsableFacade implements IResponsableFacade {

	private IResponsableDAO responsableDAO = null;

	public ResponsableFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Responsable> findAll() {
		List<Responsable> responsables = null;

		// appel à la couche DAO
		responsables = responsableDAO.findAll();

		return responsables;
	}

	public Responsable save(Responsable responsable) {
		Responsable responsableSave = responsableDAO.save(responsable);
		return responsableSave;
	}

	/**
	 * @return the responsableDAO
	 */
	public IResponsableDAO getResponsableDAO() {
		return responsableDAO;
	}

	/**
	 * @param responsableDAO the responsableDAO to set
	 */
	public void setResponsableDAO(IResponsableDAO responsableDAO) {
		this.responsableDAO = responsableDAO;
	}

	@Override
	public Responsable get(Integer id) {
		Responsable responsable = responsableDAO.get(id);
		return responsable;
	}

	@Override
	public Responsable update(Responsable responsable) {
		Responsable responsableUpdate = responsableDAO.update(responsable);
		return responsableUpdate;
	}

	@Override
	public void delete(Responsable responsable) {
		responsableDAO.delete(responsable);
	}

}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Stagiaire;

public interface IStagiaireFacade {

	List<Stagiaire> findAll();

	Stagiaire save(Stagiaire stagiaire);

	Stagiaire get(Integer id);

	Stagiaire update(Stagiaire stagiaire);

	void delete(Stagiaire stagiaire);

}

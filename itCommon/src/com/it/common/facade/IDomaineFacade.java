/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Domaine;

/**
 * @author it
 *
 */
public interface IDomaineFacade {

	List<Domaine> findAll();

	Domaine get(Integer id);

	Domaine save(Domaine domaine);

	Domaine update(Domaine domaine);

	void delete(Domaine domaine);

}

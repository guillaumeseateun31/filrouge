package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;

public interface IThemeDAO extends IDAO<Theme> {
	List<Soustheme> getSousTheme(Integer id);
}

package com.it.common.dao;

import java.util.List;

public interface IDAO<T> {

	List<T> findAll();

	T save(T bo);

	void delete(T bo);

	T update(T bo);

	T get(Integer id);
	

}
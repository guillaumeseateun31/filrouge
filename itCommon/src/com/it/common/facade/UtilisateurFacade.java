package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Utilisateur;
import com.it.common.dao.IUtilisateurDAO;

public class UtilisateurFacade implements IUtilisateurFacade {

	private IUtilisateurDAO utilisateurDAO = null;

	public UtilisateurFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Utilisateur> findAll() {
		List<Utilisateur> utilisateurs = null;

		// appel à la couche DAO
		utilisateurs = utilisateurDAO.findAll();

		return utilisateurs;
	}

	public Utilisateur save(Utilisateur utilisateur) {
		Utilisateur utilisateurSave = utilisateurDAO.save(utilisateur);
		return utilisateurSave;
	}

	/**
	 * @return the utilisateurDAO
	 */
	public IUtilisateurDAO getUtilisateurDAO() {
		return utilisateurDAO;
	}

	/**
	 * @param utilisateurDAO the utilisateurDAO to set
	 */
	public void setUtilisateurDAO(IUtilisateurDAO utilisateurDAO) {
		this.utilisateurDAO = utilisateurDAO;
	}

	@Override
	public Utilisateur get(Integer id) {
		Utilisateur utilisateur = utilisateurDAO.get(id);
		return utilisateur;
	}
	
	public Utilisateur getByEmail(String email) {
		Utilisateur utilisateur = utilisateurDAO.getByEmail(email);
		return utilisateur;
	}

	@Override
	public Utilisateur update(Utilisateur utilisateur) {
		Utilisateur utilisateurUpdate = utilisateurDAO.update(utilisateur);
		return utilisateurUpdate;
	}

	@Override
	public void delete(Utilisateur utilisateur) {
		utilisateurDAO.delete(utilisateur);
	}

}

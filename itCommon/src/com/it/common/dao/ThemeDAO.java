package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Session;
import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;

public class ThemeDAO extends AbstractDAO<Theme> implements IThemeDAO {

	
	public ThemeDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Theme> findAll() {
		List<Theme> Themes = null;

		Themes = em.createQuery("from Theme").getResultList();

		return Themes;
	}


	@Override
	public Theme get(Integer id) {
		Theme Theme = null;
		Theme = em.find(Theme.class, id);
		return Theme;
	}


	@Override
	public List<Soustheme> getSousTheme(Integer id) {
		List<Soustheme> soustheme = null;

		soustheme = em.createQuery("SELECT s from Theme t, Soustheme st, Decompositiontheme dt WHERE t.id = dt.id_theme AND dt.id_soustheme = st.id  AND t.id='" +id + "'").getResultList();

		return soustheme;
	}


}

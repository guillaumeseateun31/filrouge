package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Evaluation;

public class EvaluationDAO extends AbstractDAO<Evaluation> implements IEvaluationDAO {

	
	public EvaluationDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Evaluation> findAll() {
		List<Evaluation> Evaluations = null;

		Evaluations = em.createQuery("from Evaluation").getResultList();

		return Evaluations;
	}
	
	@Override
	public Evaluation save(Evaluation evaluation) {

		return super.save(evaluation);
	}


	@Override
	public Evaluation get(Integer id) {
		Evaluation Evaluation = null;
		Evaluation = em.find(Evaluation.class, id);
		return Evaluation;
	}


}

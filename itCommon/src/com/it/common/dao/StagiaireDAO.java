package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Stagiaire;

public class StagiaireDAO extends AbstractDAO<Stagiaire> implements IStagiaireDAO {

	
	public StagiaireDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Stagiaire> findAll() {
		List<Stagiaire> Stagiaires = null;

		Stagiaires = em.createQuery("from Stagiaire").getResultList();

		return Stagiaires;
	}

	@Override
	public Stagiaire save(Stagiaire stagiaire) {
		return super.save(stagiaire);
	}

	@Override
	public Stagiaire get(Integer id) {
		Stagiaire Stagiaire = null;
		Stagiaire = em.find(Stagiaire.class, id);
		return Stagiaire;
	}


}

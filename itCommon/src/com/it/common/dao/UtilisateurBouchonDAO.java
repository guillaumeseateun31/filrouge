package com.it.common.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.it.common.bo.Utilisateur;

public class UtilisateurBouchonDAO extends AbstractDAO<Utilisateur> implements IUtilisateurDAO {

	
	public UtilisateurBouchonDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
/*
 * private Integer id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	private Date dateDeNaissance;
	private String codePostal;
	private String adresse;
	private String ville;
	private String pays;
	private String tel;
 */
	
	@Override
	public List<Utilisateur> findAll() {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

		Utilisateur utilisateur = new Utilisateur(998, "Bob", "Bobnom", "Bobmail@mail.com", "password", null, "31100", null, null, null, null);
		utilisateurs.add(utilisateur);
		utilisateur = new Utilisateur(999, "Bob2", "Bobnom2", "Bobmail2@mail.com", "password", null, "31100", null, null, null, null);
		utilisateurs.add(utilisateur);
		return utilisateurs;
	}


	@Override
	public Utilisateur get(Integer id) {
		Utilisateur utilisateur = new Utilisateur(id, "Bob2", "Bobnom2", "Bobmail2@mail.com", "password", null, "31100", null, null, null, null);
		return utilisateur;
	}


}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Responsable implements Serializable {

	private static final long serialVersionUID = -1204354524696108656L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne @JoinColumn(name="id_type", nullable=false)
    private Typeresponsable typeresponsable;
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinTable(name = "organisation_session", joinColumns = @JoinColumn(name = "id_responsable", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_session", referencedColumnName = "id", updatable = true))
	private List<Session> session = new ArrayList<Session>();
	@ManyToOne @JoinColumn(name="id_utilisateur", nullable=false)
    private Utilisateur utilisateur;
	
	public Responsable() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Responsable(Integer id) {
		super();
		this.id = id;
	}



	

}

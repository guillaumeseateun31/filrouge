package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Salle;

public class SalleDAO extends AbstractDAO<Salle> implements ISalleDAO {

	
	public SalleDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Salle> findAll() {
		List<Salle> Salles = null;

		Salles = em.createQuery("from Salle").getResultList();

		return Salles;
	}


	@Override
	public Salle get(Integer id) {
		Salle Salle = null;
		Salle = em.find(Salle.class, id);
		return Salle;
	}


}

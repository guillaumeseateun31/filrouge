package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Utilisateurrole;

public class UtilisateurroleDAO extends AbstractDAO<Utilisateurrole> implements IUtilisateurroleDAO {

	
	public UtilisateurroleDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Utilisateurrole> findAll() {
		List<Utilisateurrole> utilisateurroles = null;

		utilisateurroles = em.createQuery("from Utilisateurrole").getResultList();

		return utilisateurroles;
	}

	public Utilisateurrole save(Utilisateurrole utilisateurrole) {

		return super.save(utilisateurrole);
	}

	@Override
	public Utilisateurrole get(Integer id) {
		Utilisateurrole utilisateurrole = null;
		utilisateurrole = em.find(Utilisateurrole.class, id);
		return utilisateurrole;
	}
	
	@Override
	public Utilisateurrole getByEmail(String email) {
		Utilisateurrole utilisateurrole = null;
		utilisateurrole = (Utilisateurrole) em.createQuery("from Utilisateurrole WHERE email = '" +email + "'").getSingleResult();
		return utilisateurrole;
	}


}

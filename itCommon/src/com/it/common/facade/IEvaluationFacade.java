package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Evaluation;

public interface IEvaluationFacade {

	List<Evaluation> findAll();

	Evaluation save(Evaluation evaluation);

	Evaluation get(Integer id);

	Evaluation update(Evaluation evaluation);

	void delete(Evaluation evaluation);

}

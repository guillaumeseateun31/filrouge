package com.it.rs.session;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.it.common.facade.ISessionFacade;
import com.it.common.bo.Session;

@Component
@Path("/session")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class SessionService {

	private ISessionFacade sessionFacade = null;

	public SessionService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("{id}")
	public Session getSession(@PathParam("id") Integer id) {
		Session session = sessionFacade.get(id);
		return session;
	}

	@GET
	@Path("/sessions")
	public List<Session> findAll() {
		List<Session> sessions = sessionFacade.findAll();
		return sessions;
	}

	@DELETE
	@Path("{id}")
	public String delete(@PathParam("id") Integer id) {
		Session session = new Session(id, null, null, null);
		sessionFacade.delete(session);
		return "ok";
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public String ajouter(Session session) {
		sessionFacade.save(session);
		return "ok";
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	public String update(Session session) {
		sessionFacade.update(session);
		return "ok";
	}

	public ISessionFacade getSessionFacade() {
		return sessionFacade;
	}

	public void setSessionFacade(ISessionFacade sessionFacade) {
		this.sessionFacade = sessionFacade;
	}



}
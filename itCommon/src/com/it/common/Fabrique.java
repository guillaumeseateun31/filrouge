package com.it.common;

import com.it.common.dao.IFormationDAO;

public class Fabrique {

	private static IFormationDAO formationDAO = new FormationDAO();
	private static IFormationDAO formationBouchonDAO = new FormationBouchonDAO();

	public IFormationDAO get(String obj) {

		switch (obj) {
		case "formationDAO":
			return formationDAO;

		case "formationBouchonDAO":
			return formationBouchonDAO;

		default:
			return null;

		}

	}
}

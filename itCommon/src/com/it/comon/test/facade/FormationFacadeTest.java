package com.it.comon.test.facade;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.it.common.bo.Formation;
import com.it.common.facade.FormationFacade;
import com.it.common.facade.IFormationFacade;

class FormationFacadeTest {
	private static IFormationFacade formationFacade = null;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// demarrage de spring
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextCommon.xml");

		formationFacade = context.getBean("formationFacade", FormationFacade.class);
	}
	
	
	@Test
	void testFindAll() {
		List<Formation> formations = formationFacade.findAll();

		// on controle le bouchon
		assertNotNull(formations);
		assertEquals(2, formations.size());
	}

}

package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Domaine;
import com.it.common.bo.Session;

public class DomaineDAO extends AbstractDAO<Domaine> implements IDomaineDAO {

	
	public DomaineDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Domaine> findAll() {
		List<Domaine> Domaines = null;

		Domaines = em.createQuery("from Domaine").getResultList();

		return Domaines;
	}


	@Override
	public Domaine get(Integer id) {
		Domaine Domaine = null;
		Domaine = em.find(Domaine.class, id);
		return Domaine;
	}
	
	



}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Formation;
import com.it.common.bo.Utilisateur;
import com.it.common.dao.IFormationDAO;

public class FormationFacade implements IFormationFacade {

	private IFormationDAO formationDAO = null;

	public FormationFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Formation> findAll() {
		List<Formation> formations = null;

		// appel à la couche DAO
		formations = formationDAO.findAll();

		return formations;
	}

	@Override
	public Formation get(Integer id) {
		Formation formation = formationDAO.get(id);
		return formation;
	}

	@Override
	public Formation save(Formation formation) {
		Formation formationSave = formationDAO.save(formation);
		return formationSave;
	}

	@Override
	public Formation update(Formation formation) {
		Formation formationUpdate = formationDAO.update(formation);
		return formationUpdate;
	}

	@Override
	public void delete(Formation formation) {
		formationDAO.delete(formation);
		
	}

	public IFormationDAO getFormationDAO() {
		return formationDAO;
	}

	public void setFormationDAO(IFormationDAO formationDAO) {
		this.formationDAO = formationDAO;
	}





}

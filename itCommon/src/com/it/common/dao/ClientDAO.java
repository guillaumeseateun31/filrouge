package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Client;
import com.it.common.bo.Stagiaire;

public class ClientDAO extends AbstractDAO<Client> implements IClientDAO {

	
	public ClientDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Client> findAll() {
		List<Client> Clients = null;

		Clients = em.createQuery("from Client").getResultList();

		return Clients;
	}
	
	@Override
	public Client save(Client client) {

		return super.save(client);
	}


	@Override
	public Client get(Integer id) {
		Client Client = null;
		Client = em.find(Client.class, id);
		return Client;
	}


}

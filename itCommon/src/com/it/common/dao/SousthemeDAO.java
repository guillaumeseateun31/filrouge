package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Soustheme;

public class SousthemeDAO extends AbstractDAO<Soustheme> implements ISousthemeDAO {

	
	public SousthemeDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Soustheme> findAll() {
		List<Soustheme> Sousthemes = null;

		Sousthemes = em.createQuery("from Soustheme").getResultList();

		return Sousthemes;
	}


	@Override
	public Soustheme get(Integer id) {
		Soustheme Soustheme = null;
		Soustheme = em.find(Soustheme.class, id);
		return Soustheme;
	}


}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@Table(name = "programme_formation")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Programmeformation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private Integer id;
	
	@ManyToOne @JoinColumn(name="id_formation", nullable=false)
    private Formation formation;

	@ManyToOne @JoinColumn(name="id_soustheme", nullable=false)
    private Soustheme soustheme;

}

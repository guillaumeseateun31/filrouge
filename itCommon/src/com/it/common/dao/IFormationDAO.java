package com.it.common.dao;

import com.it.common.bo.Formation;

public interface IFormationDAO extends IDAO<Formation> {

}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Domaine;
import com.it.common.dao.IDomaineDAO;

public class DomaineFacade implements IDomaineFacade {

	private IDomaineDAO domaineDAO = null;

	public DomaineFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Domaine> findAll() {
		List<Domaine> domaines = null;

		// appel à la couche DAO
		domaines = domaineDAO.findAll();

		return domaines;
	}

	@Override
	public Domaine get(Integer id) {
		Domaine domaine = domaineDAO.get(id);
		return domaine;
	}

	@Override
	public Domaine save(Domaine domaine) {
		Domaine domaineSave = domaineDAO.save(domaine);
		return domaineSave;
	}

	@Override
	public Domaine update(Domaine domaine) {
		Domaine domaineUpdate = domaineDAO.update(domaine);
		return domaineUpdate;
	}

	@Override
	public void delete(Domaine domaine) {
		domaineDAO.delete(domaine);
		
	}

	public IDomaineDAO getDomaineDAO() {
		return domaineDAO;
	}

	public void setDomaineDAO(IDomaineDAO domaineDAO) {
		this.domaineDAO = domaineDAO;
	}



}

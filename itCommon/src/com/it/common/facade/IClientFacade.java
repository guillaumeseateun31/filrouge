package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Client;

public interface IClientFacade {

	List<Client> findAll();

	Client save(Client client);

	Client get(Integer id);

	Client update(Client client);

	void delete(Client client);

}

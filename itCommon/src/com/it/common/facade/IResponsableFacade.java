package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Responsable;

public interface IResponsableFacade {

	List<Responsable> findAll();

	Responsable save(Responsable responsable);

	Responsable get(Integer id);

	Responsable update(Responsable responsable);

	void delete(Responsable responsable);

}

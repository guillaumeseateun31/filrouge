package com.it.common.dao;

import com.it.common.bo.Formateur;

public interface IFormateurDAO extends IDAO<Formateur> {

}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Session;
import com.it.common.dao.ISessionDAO;

public class SessionFacade implements ISessionFacade {

	private ISessionDAO sessionDAO = null;

	public SessionFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Session> findAll() {
		List<Session> sessions = null;

		// appel à la couche DAO
		sessions = sessionDAO.findAll();

		return sessions;
	}

	@Override
	public Session get(Integer id) {
		Session session = sessionDAO.get(id);
		return session;
	}

	@Override
	public Session save(Session session) {
		Session sessionSave = sessionDAO.save(session);
		return sessionSave;
	}

	@Override
	public Session update(Session session) {
		Session sessionUpdate = sessionDAO.update(session);
		return sessionUpdate;
	}

	@Override
	public void delete(Session session) {
		sessionDAO.delete(session);
		
	}

	public ISessionDAO getSessionDAO() {
		return sessionDAO;
	}

	public void setSessionDAO(ISessionDAO sessionDAO) {
		this.sessionDAO = sessionDAO;
	}

	@Override
	public List<Session> getSessionByDomaine(Integer id_domaine) {
		List<Session> sessions = null;

		// appel à la couche DAO
		sessions = sessionDAO.getAllByDomaine(id_domaine);

		return sessions;
	}



}

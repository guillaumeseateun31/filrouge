package com.it.common.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Evaluation implements Serializable {

	private static final long serialVersionUID = 4826996683471351948L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Integer noteFormateur;
	private Integer noteSession;
	private Integer noteAccueil;
	private String souhait;
	private String projet;
	private String signature;
	
	public Evaluation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Evaluation(Integer id, Integer noteFormateur, Integer noteSession, Integer noteAccueil, String souhait,
			String projet, String signature) {
		super();
		this.id = id;
		this.noteFormateur = noteFormateur;
		this.noteSession = noteSession;
		this.noteAccueil = noteAccueil;
		this.souhait = souhait;
		this.projet = projet;
		this.signature = signature;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNoteFormateur() {
		return noteFormateur;
	}

	public void setNoteFormateur(Integer noteFormateur) {
		this.noteFormateur = noteFormateur;
	}

	public Integer getNoteSession() {
		return noteSession;
	}

	public void setNoteSession(Integer noteSession) {
		this.noteSession = noteSession;
	}

	public Integer getNoteAccueil() {
		return noteAccueil;
	}

	public void setNoteAccueil(Integer noteAccueil) {
		this.noteAccueil = noteAccueil;
	}

	public String getSouhait() {
		return souhait;
	}

	public void setSouhait(String souhait) {
		this.souhait = souhait;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	

}

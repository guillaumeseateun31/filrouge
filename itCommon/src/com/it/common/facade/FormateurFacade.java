package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Formateur;
import com.it.common.dao.IFormateurDAO;

public class FormateurFacade implements IFormateurFacade {

	private IFormateurDAO formateurDAO = null;

	public FormateurFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Formateur> findAll() {
		List<Formateur> formateurs = null;

		// appel à la couche DAO
		formateurs = formateurDAO.findAll();

		return formateurs;
	}

	public Formateur save(Formateur formateur) {
		Formateur formateurSave = formateurDAO.save(formateur);
		return formateurSave;
	}

	/**
	 * @return the formateurDAO
	 */
	public IFormateurDAO getFormateurDAO() {
		return formateurDAO;
	}

	/**
	 * @param formateurDAO the formateurDAO to set
	 */
	public void setFormateurDAO(IFormateurDAO formateurDAO) {
		this.formateurDAO = formateurDAO;
	}

	@Override
	public Formateur get(Integer id) {
		Formateur formateur = formateurDAO.get(id);
		return formateur;
	}

	@Override
	public Formateur update(Formateur formateur) {
		Formateur formateurUpdate = formateurDAO.update(formateur);
		return formateurUpdate;
	}

	@Override
	public void delete(Formateur formateur) {
		formateurDAO.delete(formateur);
	}

}

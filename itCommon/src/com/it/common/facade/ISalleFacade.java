/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Salle;

/**
 * @author it
 *
 */
public interface ISalleFacade {

	List<Salle> findAll();

	Salle get(Integer id);

	Salle save(Salle salle);

	Salle update(Salle salle);

	void delete(Salle salle);

}

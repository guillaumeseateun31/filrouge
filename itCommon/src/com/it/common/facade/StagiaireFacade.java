package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Stagiaire;
import com.it.common.dao.IStagiaireDAO;

public class StagiaireFacade implements IStagiaireFacade {

	private IStagiaireDAO stagiaireDAO = null;

	public StagiaireFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Stagiaire> findAll() {
		List<Stagiaire> stagiaires = null;

		// appel à la couche DAO
		stagiaires = stagiaireDAO.findAll();

		return stagiaires;
	}

	public Stagiaire save(Stagiaire stagiaire) {
		Stagiaire stagiaireSave = stagiaireDAO.save(stagiaire);
		return stagiaireSave;
	}

	/**
	 * @return the stagiaireDAO
	 */
	public IStagiaireDAO getStagiaireDAO() {
		return stagiaireDAO;
	}

	/**
	 * @param stagiaireDAO the stagiaireDAO to set
	 */
	public void setStagiaireDAO(IStagiaireDAO stagiaireDAO) {
		this.stagiaireDAO = stagiaireDAO;
	}

	@Override
	public Stagiaire get(Integer id) {
		Stagiaire stagiaire = stagiaireDAO.get(id);
		return stagiaire;
	}

	@Override
	public Stagiaire update(Stagiaire stagiaire) {
		Stagiaire stagiaireUpdate = stagiaireDAO.update(stagiaire);
		return stagiaireUpdate;
	}

	@Override
	public void delete(Stagiaire stagiaire) {
		stagiaireDAO.delete(stagiaire);
	}

}

package com.it.comon.test.facade;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.it.common.bo.Domaine;
import com.it.common.facade.DomaineFacade;
import com.it.common.facade.IDomaineFacade;

class DomaineFacadeTest {
	private static IDomaineFacade domaineFacade = null;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// demarrage de spring
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContextCommon.xml");

		domaineFacade = context.getBean("domaineFacade", DomaineFacade.class);
	}
	
	
	@Test
	void testFindAll() {
		List<Domaine> domaines = domaineFacade.findAll();

		// on controle le bouchon
		assertNotNull(domaines);
		//assertEquals(2, domaines.size());
	}
	
	@Test
	void testGet() {
		Domaine domaine = domaineFacade.get(1);

		// on controle le bouchon
		assertNotNull(domaine);
		//assertEquals(2, domaines.size());
	}

}

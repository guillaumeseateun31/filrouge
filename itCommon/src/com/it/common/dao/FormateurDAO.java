package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Formateur;
import com.it.common.bo.Stagiaire;

public class FormateurDAO extends AbstractDAO<Formateur> implements IFormateurDAO {

	
	public FormateurDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Formateur> findAll() {
		List<Formateur> Formateurs = null;

		Formateurs = em.createQuery("from Formateur").getResultList();

		return Formateurs;
	}
	
	@Override
	public Formateur save(Formateur formateur) {

		return super.save(formateur);
	}


	@Override
	public Formateur get(Integer id) {
		Formateur Formateur = null;
		Formateur = em.find(Formateur.class, id);
		return Formateur;
	}


}

package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Assistersession;
import com.it.common.bo.Session;
import com.it.common.bo.Utilisateur;

public class AssistersessionDAO extends AbstractDAO<Assistersession> implements IAssistersessionDAO {

	
	public AssistersessionDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Assistersession> findAll() {
		List<Assistersession> assistersessions = null;

		assistersessions = em.createQuery("from Assistersession").getResultList();

		return assistersessions;
	}


	@Override
	public Assistersession get(Integer id) {
		Assistersession assistersession = null;
		assistersession = em.find(Assistersession.class, id);
		return assistersession;
	}
	
	public List<Assistersession> getByUser(Integer id) {
		List<Assistersession> assistersessions = null;

		//assistersessions = em.createQuery("SELECT as from Assistersession as, Utilisateur u, Stagiaire s, Evaluation e WHERE as.id_stagiaire = s.id AND s.id_utilisateur = u.id AND e.id = as.id_evaluation AND u.id='" +id + "'").getResultList();
		assistersessions = em.createQuery("SELECT a from Assistersession a, Utilisateur u, Stagiaire s WHERE a.stagiaire.id = s.id AND s.utilisateur.id = u.id  AND u.id='" +id + "'").getResultList();
		return assistersessions;
	}
	
	
	public Assistersession save(Assistersession assistersession) {
		return super.save(assistersession);
	}
	
	



}

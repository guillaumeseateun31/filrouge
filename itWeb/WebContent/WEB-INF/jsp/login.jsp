<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<%@include file="head/head.jsp" %>
</head>
<body onload="initPage()">
	<div id="page">
		<jsp:include page="header/header.jsp"></jsp:include>
		<%@include file="nav/nav.jsp" %>
		<div id="content">
    <div class="modal-content">
      <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-title text-center">
          <h4>Connexion</h4>
        </div>
        <div class="d-flex flex-column text-center">
          <form action="j_security_check" method="post" id="formLoginHeader">
           <p class="erreur"><c:out value="${requestScope.erreur }" escapeXml="false"></c:out> </p>
            <div class="form-group">
              <input type="email" name="j_username" class="form-control" id="email1" placeholder="Votre adresse mail ...">
            </div>
            <div class="form-group">
              <input type="password" name="j_password" class="form-control" id="password1" placeholder="Votre mot de passe ...">
            </div>
            <button type="submit" type="button" class="btn btn-success btn-block btn-round">Se connecter</button>
          </form>

      </div>
    </div>
      <div class="modal-footer d-flex justify-content-center">
        <div class="signup-section">Pas encore inscrit ? <button id="connecttologin" class="btn btn-primary"  data-dismiss="modal"> S'inscrire </button></div>
      </div>
  </div>
		</div>
		<jsp:include page="footer/footer.jsp"></jsp:include>
	</div>
	

</body>
</html>
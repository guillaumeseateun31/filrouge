package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Utilisateurrole;
import com.it.common.dao.IUtilisateurroleDAO;

public class UtilisateurroleFacade implements IUtilisateurroleFacade {

	private IUtilisateurroleDAO utilisateurroleDAO = null;

	public UtilisateurroleFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Utilisateurrole> findAll() {
		List<Utilisateurrole> utilisateurroles = null;

		// appel à la couche DAO
		utilisateurroles = utilisateurroleDAO.findAll();

		return utilisateurroles;
	}

	public Utilisateurrole save(Utilisateurrole utilisateurrole) {
		Utilisateurrole utilisateurroleSave = utilisateurroleDAO.save(utilisateurrole);
		return utilisateurroleSave;
	}

	/**
	 * @return the utilisateurroleDAO
	 */
	public IUtilisateurroleDAO getUtilisateurroleDAO() {
		return utilisateurroleDAO;
	}

	/**
	 * @param utilisateurroleDAO the utilisateurroleDAO to set
	 */
	public void setUtilisateurroleDAO(IUtilisateurroleDAO utilisateurroleDAO) {
		this.utilisateurroleDAO = utilisateurroleDAO;
	}

	@Override
	public Utilisateurrole get(Integer id) {
		Utilisateurrole utilisateurrole = utilisateurroleDAO.get(id);
		return utilisateurrole;
	}
	
	public Utilisateurrole getByEmail(String email) {
		Utilisateurrole utilisateurrole = utilisateurroleDAO.getByEmail(email);
		return utilisateurrole;
	}

	@Override
	public Utilisateurrole update(Utilisateurrole utilisateurrole) {
		Utilisateurrole utilisateurroleUpdate = utilisateurroleDAO.update(utilisateurrole);
		return utilisateurroleUpdate;
	}

	@Override
	public void delete(Utilisateurrole utilisateurrole) {
		utilisateurroleDAO.delete(utilisateurrole);
	}

}

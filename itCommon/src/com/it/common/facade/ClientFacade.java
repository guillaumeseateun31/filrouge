package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Client;
import com.it.common.dao.IClientDAO;

public class ClientFacade implements IClientFacade {

	private IClientDAO clientDAO = null;

	public ClientFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Client> findAll() {
		List<Client> clients = null;

		// appel à la couche DAO
		clients = clientDAO.findAll();

		return clients;
	}

	public Client save(Client client) {
		Client clientSave = clientDAO.save(client);
		return clientSave;
	}

	/**
	 * @return the clientDAO
	 */
	public IClientDAO getClientDAO() {
		return clientDAO;
	}

	/**
	 * @param clientDAO the clientDAO to set
	 */
	public void setClientDAO(IClientDAO clientDAO) {
		this.clientDAO = clientDAO;
	}

	@Override
	public Client get(Integer id) {
		Client client = clientDAO.get(id);
		return client;
	}

	@Override
	public Client update(Client client) {
		Client clientUpdate = clientDAO.update(client);
		return clientUpdate;
	}

	@Override
	public void delete(Client client) {
		clientDAO.delete(client);
	}

}

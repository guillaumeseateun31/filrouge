package com.it.web.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.it.common.bo.Assistersession;
import com.it.common.bo.Evaluation;
import com.it.common.bo.Utilisateur;
import com.it.common.facade.IAssistersessionFacade;
import com.it.common.facade.IEvaluationFacade;
import com.it.common.facade.IUtilisateurFacade;

/**
 * Servlet implementation class Accueil
 */
@WebServlet({ "/signature" })
public class Signature extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IUtilisateurFacade utilisateurFacade;
	private IEvaluationFacade evaluationFacade;
	private IAssistersessionFacade assistersessionFacade;

	/**
	 * Default constructor.
	 */
	public Signature() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/signature.jsp");

		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/sessions.jsp");

		// recuperation du nom
		String id_evaluation = request.getParameter("id_evaluation");
		
		Evaluation evaluation = evaluationFacade.get(Integer.parseInt(id_evaluation));
		evaluation.setSignature("sign�");
		evaluationFacade.update(evaluation);
		 
		
		

		Utilisateur user = utilisateurFacade.getByEmail(request.getUserPrincipal().getName());
		List<Assistersession> assistersessions = assistersessionFacade.getByUser(user.getId());
		//List<Assistersession> assistersessions = assistersessionFacade.findAll();
		request.setAttribute("assistersessions", assistersessions);
		
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);
	}

	public IEvaluationFacade getEvaluationFacade() {
		return evaluationFacade;
	}

	@Autowired
	public void setEvaluationFacade(IEvaluationFacade evaluationFacade) {
		this.evaluationFacade = evaluationFacade;
	}

	/**
	 * @return the utilisateurFacade
	 */
	public IUtilisateurFacade getUtilisateurFacade() {
		return utilisateurFacade;
	}

	/**
	 * @param utilisateurFacade the utilisateurFacade to set
	 */
	@Autowired
	public void setUtilisateurFacade(IUtilisateurFacade utilisateurFacade) {
		this.utilisateurFacade = utilisateurFacade;
	}

	public IAssistersessionFacade getAssistersessionFacade() {
		return assistersessionFacade;
	}

	@Autowired
	public void setAssistersessionFacade(IAssistersessionFacade assistersessionFacade) {
		this.assistersessionFacade = assistersessionFacade;
	}
	
	

}

package com.it.common.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Stagiaire implements Serializable {

	private static final long serialVersionUID = -8468525540462834049L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private Boolean isCandidat;
	@ManyToOne @JoinColumn(name="id_utilisateur", nullable=false)
    private Utilisateur utilisateur;
	
	public Stagiaire() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Stagiaire(Integer id, Boolean isCandidat, Utilisateur utilisateur) {
		super();
		this.id = id;
		this.isCandidat = isCandidat;
		this.utilisateur = utilisateur;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getIsCandidat() {
		return isCandidat;
	}

	public void setIsCandidat(Boolean isCandidat) {
		this.isCandidat = isCandidat;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}
	
	


}

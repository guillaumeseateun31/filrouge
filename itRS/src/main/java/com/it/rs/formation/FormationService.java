package com.it.rs.formation;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.it.common.facade.IFormationFacade;
import com.it.common.bo.Formation;

@Component
@Path("/formation")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class FormationService {

	private IFormationFacade formationFacade = null;

	public FormationService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("{id}")
	public Formation getFormation(@PathParam("id") Integer id) {
		Formation formation = formationFacade.get(id);
		return formation;
	}

	@GET
	@Path("/formations")
	public List<Formation> findAll() {
		List<Formation> formations = formationFacade.findAll();
		return formations;
	}

	@DELETE
	@Path("{id}")
	public String delete(@PathParam("id") Integer id) {
		Formation formation = new Formation(id, null, null, null, null, null, null, false);
		formationFacade.delete(formation);
		return "ok";
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public String ajouter(Formation formation) {
		formationFacade.save(formation);
		return "ok";
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	public String update(Formation formation) {
		formationFacade.update(formation);
		return "ok";
	}

	public IFormationFacade getFormationFacade() {
		return formationFacade;
	}

	public void setFormationFacade(IFormationFacade formationFacade) {
		this.formationFacade = formationFacade;
	}



}
package com.it.common.advice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;

import com.it.common.bo.Formation;

public class LoggerAdvice {

	private static final Logger logger = LogManager.getLogger(LoggerAdvice.class);

	public void logBefore() {
		logger.debug("Avant appel méthode");
	}

	public Formation logAroundAvecParam(ProceedingJoinPoint pjp, Integer id) {
		Formation livre = null;
		logger.debug("livre demande :" + id);
		List<Object> livreArgs = new ArrayList<Object>();
		livreArgs.add(new Integer(id));
		// appel de la vraie methode
		try {
			livre = (Formation) pjp.proceed(livreArgs.toArray());
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return livre;
	}

	public List<Formation> logAround(ProceedingJoinPoint pjp) {
		logger.debug("Avant appel méthode AROUND");

		LocalDateTime debutMethode = LocalDateTime.now();
		LocalDateTime finMethode;

		List<Formation> livres = null;
		// appel de la vraie methode
		try {
			livres = (List<Formation>) pjp.proceed();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * Livre livre = new Livre(1, "125fdg566", "livre de 1 AOP" +
		 * pjp.getSignature(), new Date()); livres.add(livre);
		 */
		logger.debug("Après appel méthode AROUND");

		finMethode = LocalDateTime.now();

		logger.debug("tps appel methode :" + (String.valueOf(finMethode.getSecond() - debutMethode.getSecond()))
				+ " secondes");

		return livres;
	}

	public void logAfter() {
		logger.debug("Après appel méthode");
	}
}

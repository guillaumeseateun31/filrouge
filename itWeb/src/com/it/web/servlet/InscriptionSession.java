package com.it.web.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.it.common.bo.Assistersession;
import com.it.common.bo.Domaine;
import com.it.common.bo.Evaluation;
import com.it.common.bo.Formation;
import com.it.common.bo.Session;
import com.it.common.bo.Stagiaire;
import com.it.common.bo.Theme;
import com.it.common.bo.Utilisateur;
import com.it.common.facade.IAssistersessionFacade;
import com.it.common.facade.IDomaineFacade;
import com.it.common.facade.IEvaluationFacade;
import com.it.common.facade.IFormationFacade;
import com.it.common.facade.ISessionFacade;
import com.it.common.facade.IStagiaireFacade;
import com.it.common.facade.IThemeFacade;
import com.it.common.facade.IUtilisateurFacade;
import com.it.common.facade.UtilisateurFacade;


@WebServlet({ "/inscriptionsession" })
public class InscriptionSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(InscriptionSession.class);

	private IFormationFacade formationFacade;
	private IDomaineFacade domaineFacade;
	private ISessionFacade sessionFacade;
	private IThemeFacade themeFacade;
	private IStagiaireFacade stagiaireFacade;
	private IUtilisateurFacade utilisateurFacade;
	private IEvaluationFacade evaluationFacade;
	private IAssistersessionFacade assistersessionFacade;

	/**
	 * Default constructor.
	 */
	public InscriptionSession() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/inscriptionsession.jsp");

		// partage par toutes les servlet
		getServletContext().setAttribute("monInfoPartagee", "l info");

		List<Formation> formations = formationFacade.findAll();

		// passage des livres dans la requete pour la view
		request.setAttribute("formations", formations);
		
		// themes 
		int themeNumber = Integer.parseInt(request.getParameter("theme"));
		Theme themeSelected = themeFacade.get(themeNumber);
		request.setAttribute("themeSelected", themeSelected);
		
		// domaines
		/*int domaineNumber = Integer.parseInt(request.getParameter("domaine"));
		Domaine domaineSelected = domaineFacade.get(domaineNumber);
		request.setAttribute("domaineSelected", domaineSelected);
		*/
		
		
		// sessions
		//List<Session> sessions = sessionFacade.getSessionByDomaine(domaineSelected.getId());
		//request.setAttribute("sessions", sessions);
		
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);
		
		logger.debug("fin du doGet");

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String erreur = "";
		// recuperation du dispatcher
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/jsp/sessions.jsp");

		// recuperation du id user
		String id_user = request.getParameter("id_user");
		// recuperation de id session
		String id_session = request.getParameter("id_session");
		Session session = sessionFacade.get(Integer.parseInt(id_session));
		
		Utilisateur utilisateur = utilisateurFacade.get(Integer.parseInt(id_user));
		// creation de l utilisateur
		Stagiaire stagiaire = stagiaireFacade.get(Integer.parseInt(id_user));
		if(stagiaire == null) {
			stagiaire = new Stagiaire(null, true, utilisateur);
		
			stagiaireFacade.save(stagiaire);
		}
		
		Evaluation evaluation = null;
		evaluation = new Evaluation(null, null, null, null, null, null, null);
		Evaluation evaluationObj = evaluationFacade.save(evaluation);
		Assistersession assistersession = new Assistersession(null, session, stagiaire, evaluation);
		assistersessionFacade.save(assistersession);
		
		
		
		Utilisateur user = utilisateurFacade.getByEmail(request.getUserPrincipal().getName());
		List<Assistersession> assistersessions = assistersessionFacade.getByUser(user.getId());
		//List<Assistersession> assistersessions = assistersessionFacade.findAll();
		request.setAttribute("assistersessions", assistersessions);
		/*
		 * request.getSession().setAttribute("nom", nom);
		 * request.getSession().setAttribute("prenom", prenom);
		 */
		// forward vers la jsp de la reponse
		requestDispatcher.forward(request, response);
	}
	

	/**
	 * @return the livreAC
	 */
	public IFormationFacade getFormationFacade() {
		return formationFacade;
	}

	/**
	 * @param livreAC the livreAC to set
	 */
	@Autowired
	public void setFormationFacade(IFormationFacade formationFacade) {
		this.formationFacade = formationFacade;
	}

	public IDomaineFacade getDomaineFacade() {
		return domaineFacade;
	}

	@Autowired
	public void setDomaineFacade(IDomaineFacade domaineFacade) {
		this.domaineFacade = domaineFacade;
	}

	public ISessionFacade getSessionFacade() {
		return sessionFacade;
	}

	@Autowired
	public void setSessionFacade(ISessionFacade sessionFacade) {
		this.sessionFacade = sessionFacade;
	}

	public IThemeFacade getThemeFacade() {
		return themeFacade;
	}

	@Autowired
	public void setThemeFacade(IThemeFacade themeFacade) {
		this.themeFacade = themeFacade;
	}

	public IStagiaireFacade getStagiaireFacade() {
		return stagiaireFacade;
	}

	@Autowired
	public void setStagiaireFacade(IStagiaireFacade stagiaireFacade) {
		this.stagiaireFacade = stagiaireFacade;
	}

	public IUtilisateurFacade getUtilisateurFacade() {
		return utilisateurFacade;
	}

	@Autowired
	public void setUtilisateurFacade(IUtilisateurFacade utilisateurFacade) {
		this.utilisateurFacade = utilisateurFacade;
	}

	public IEvaluationFacade getEvaluationFacade() {
		return evaluationFacade;
	}

	@Autowired
	public void setEvaluationFacade(IEvaluationFacade evaluationFacade) {
		this.evaluationFacade = evaluationFacade;
	}

	public IAssistersessionFacade getAssistersessionFacade() {
		return assistersessionFacade;
	}

	@Autowired
	public void setAssistersessionFacade(IAssistersessionFacade assistersessionFacade) {
		this.assistersessionFacade = assistersessionFacade;
	}
	
	
	

}

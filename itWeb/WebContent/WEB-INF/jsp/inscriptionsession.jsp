<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<title>Inscription session</title>
<%@include file="head/head.jsp" %>
<link rel="stylesheet" href="css/bd-wizard.css">
</head>
<body onload="initPage()">
	<div id="page">
		<jsp:include page="header/header.jsp"></jsp:include>
		<%@include file="nav/nav.jsp" %>
		<div id="content" style="margin-top:5em;margin-bottom:5em;">
			<div class="container">
        <div id="wizard">
          <h3>Selection session</h3>
         
          <section>
           <h5 class="bd-wizard-step-title">Etape 1</h5>

		<c:choose>
			  <c:when test = "${empty themeSelected.sessions }">
			   <p><h3> Aucune session de prévue</h3><p>
			  </c:when>

			  <c:otherwise>
			   
			
				           <h2 class="section-heading">Sélectionnez la session en <span class="text-success">${themeSelected.nom }</span></h2>
           <p> Ci-dessous les sessions prochaines sessions disponibles pour la spécialité.</p>
           <div id="step1" class="purpose-radios-wrapper">
              
              <c:forEach items="${themeSelected.sessions }" var="session" varStatus="status">

			      <div class="purpose-radio">
                   <input type="radio" name="purpose" id="test${status.index}" class="purpose-radio-input" value="${session.id }">
                  <label for="test${status.index}" class="purpose-radio-label">
                    <span class="label-icon">
                      <p class="text-center font-weight-bold session" > ${fn:toUpperCase(session.nom)} </p>
                    </span>
                    <p class="text-center ville"><span class="label-text">${session.salle.ville }</span></p>
                    <input type="hidden" class="id_sessionForm" value="${session.id }">
                    <p class="text-center date"><span class="label-text text-center"><fmt:formatDate pattern = "dd/MM/yyyy"  value = "${session.dateDebut }" /> - <fmt:formatDate pattern = "dd/MM/yyyy" value = "${session.dateFin }" />  </span></p>
                    <c:set var="prix" value="0" />
                    <c:set var="sousthemesC" value="" />
                    <p class="text-center sousthemes" style="margin-left:1em;margin-right:1em;">
                    	
                    	<c:forEach items="${session.formation.sousthemes }" var="soustheme">
                    	<c:set var="prix" value="${prix + soustheme.coutHT}"/>
                    	<c:set var="sousthemesC" value="${sousthemesC} #${soustheme.nom}"/>
                    	</c:forEach>
                    	<span class="label-text" style="color:blue;"><c:out value="${sousthemesC }"></c:out></span>
                    </p>
                    <fmt:setLocale value = "fr_FR"/>
                    <p class="text-center font-weight-bold prix"><fmt:formatNumber value="${prix}" 
            type="currency" currencySymbol="€"  /> </p>
                  </label>
                 </div>
				
	            </c:forEach>
           </div>
          </section>
          <h3>Etape 2 détail du compte</h3>
          <section>
            <h5 class="bd-wizard-step-title">Etape 2</h5>
            <h2 class="section-heading">Entrez vos coordonées </h2>
            <div class="form-group">
              <label for="firstName" class="sr-only">Prénom</label>
              <input type="text" name="firstName" id="prenomC" class="form-control" placeholder="First Name" value="<c:out value="${user.prenom }"></c:out>">
            </div>
            <div class="form-group">
              <label for="lastName" class="sr-only">Nom</label>
              <input type="text" name="lastName" id="nomC" class="form-control" placeholder="Last Name" value="<c:out value="${user.nom }"></c:out>">
            </div>
            <div class="form-group">
              <label for="emailAddress" class="sr-only">Email</label>
              <input type="email" name="emailAddress" id="emailC" class="form-control" placeholder="Email Address" value="<c:out value="${pageContext.request.userPrincipal.name }"></c:out>">
            </div>
          </section>
          <h3>Etape 3 </h3>
          <section>
              <h5 class="bd-wizard-step-title">Etape 3</h5>
              <h2 class="section-heading mb-5">Récapitulatif </h2>
              <h6 class="font-weight-bold" >Session sélectionnée</h6>
              <p style="margin:0;" id="sessionRecap">Non sélectionné</p>
              <p style="margin:0;">Ville : <span id="villeRecap">Non sélectionné</span> </p>
              <p style="margin:0;">Date : <span id="dateRecap">Non sélectionné</span> </p>
              <p style="margin:0;" class="mb-4">Modules : <span id="sousthemesRecap" style="color:blue;">Non sélectionné</span> </p>
              <h6 class="font-weight-bold">Détail de votre compte</h6>
              <p class="mb-4"><span id="prenomRecap">Cha</span> <span id="nomRecap">Ji-Hun C</span> <br>
                  Email: <span id="emailRecap">willms_abby@gmail.com</span></p>
               <h6 class="font-weight-bold">Total à régler</h6>
              <p class="mb-4"><span id="prixRecap">Non sélectionné </span></p>

          </section>
          
	            
            </c:otherwise>
		</c:choose>

        </div>
    </div>
  <form action="inscriptionsession" method="post" id="sessionForm">
 	<input type="hidden" name="id_user" value="<c:out value="${user.id }"></c:out>">
 	<input id="id_session" type="hidden" name="id_session" value="">
  </form>  
		
		</div>		
		<jsp:include page="footer/footer.jsp"></jsp:include>
		<script src="js/jquery.steps.js"></script>
		<script src="js/bd-wizard.js"></script>
		
	</div>
	<script>
$( document ).ready(function() {

	$('a[href=#finish]').attr('id', 'finish');
	$('a[href=#next]:nth-child(1)').attr('id', 'second');
	
   $(".active").removeClass("active");
   $("#formations").addClass("active");

   $("#step1").on("click", function () {
	   if ($("#step1 input:radio[name=purpose]").is(":checked")) {
		   var selection = $('input[name=purpose]:checked', '#step1');
		   var date = selection.siblings('label').children(".date").children("span").html();
		   var ville = selection.siblings('label').children(".ville").children("span").html();
		   var sousthemes = selection.siblings('label').children(".sousthemes").children("span").html();
		   var prix = selection.siblings('label').children(".prix").html();
		   var session = selection.siblings('label').children("span").children(".session").html();
		   //var ville = selection.siblings('label').children("#id_sessionForm").val();

		   $("#sessionRecap").html(session);
		   $("#dateRecap").html(date);
		   $("#villeRecap").html(ville);
		   $("#sousthemesRecap").html(sousthemes);
		   $("#prixRecap").html(prix);
	       
	    }
   });

   $("#second").on("click", function () {
	   var selection = $('input[name=purpose]:checked', '#step1');
	   var id_session = selection.siblings('label').children(".id_sessionForm").val();
	   $("#prenomRecap").html($("#prenomC").val());
	   $("#nomRecap").html($("#nomC").val());
	   $("#emailRecap").html($("#emailC").val());
	   $("#id_session").val(id_session);
   });

   $("#finish").on("click", function () {
	   $("#sessionForm").submit();
   });
   
});
</script>
</body>
</html>
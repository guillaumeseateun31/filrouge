package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Utilisateur;

public interface IUtilisateurFacade {

	List<Utilisateur> findAll();

	Utilisateur save(Utilisateur utilisateur);

	Utilisateur get(Integer id);
	
	Utilisateur getByEmail(String email);

	Utilisateur update(Utilisateur utilisateur);

	void delete(Utilisateur utilisateur);

}

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:setBundle basename="messages"></fmt:setBundle>
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="index.html"><img src="images/Logo_IT.png" class="logo" alt="LOGO"> </a>
      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse"
        data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav nav ml-auto">
          <li class="nav-item" id="accueil"><a href="accueil" class="nav-link" data-nav-section="home" ><span>Accueil</span></a></li>
          <!--  <li class="nav-item" id="nous"><a href="#" class="nav-link" data-nav-section="about" ><span>Nous</span></a></li>-->
          <li class="nav-item" id="formations"><a href="formations" class="nav-link" data-nav-section="formations" ><span>Nos formations</span></a>
          </li>
         <!--  <li class="nav-item" id="agences"><a href="#" class="nav-link" data-nav-section="team" ><span>Agences</span></a></li>-->
          <li class="nav-item" id="contact"><a href="#" class="nav-link" data-nav-section="contact" ><span>Contact</span></a></li>
          <c:if test="${empty pageContext.request.userPrincipal }">
          	<!-- <li class="nav-item cta" ><button class="nav-link btn-it" data-toggle="modal" data-target="#loginModal" id="login">S'identifer</button></li>-->
          	<a id="btnLogin" href="sessions" class="nav-link btn-it">S'identifier</a>
          </c:if>
          <c:if test="${not empty pageContext.request.userPrincipal }">
          			<li class="nav-item" id="sessions"><a href="sessions" class="nav-link" data-nav-section="sessions" ><span>Sessions</span></a></li>
				 <li class="nav-item" id="ffg"><a href="#" class="nav-link" data-nav-section="contact" ><span style="color:grey;"><c:out value="${pageContext.request.userPrincipal.name }"></c:out></span></a></li>
				 <li class="nav-item" id="qsd"></li>
				 <li class="nav-item cta" ><a id="btnDeconnexion" href="deconnexion" class="nav-link btn-it">Deconnexion</a></li>
			</c:if>
           



        </ul>
      </div>
    </div>
  </nav>
  
    <!-- <section class="hero-wrap" style="background-image: url('images/machine-learning-et-big-data.jpg'); height: 170px;"
    data-section="home"> 
    -->
    <section class="d-none d-md-none d-lg-block" style="background-image: url('images/machine-learning-et-big-data.jpg'); background-size: cover;  height: 170px;">
    <div class="overlay"></div>
    
  </section>
  
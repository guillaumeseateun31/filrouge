<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% pageContext.setAttribute("newLineChar", "</br> •"); %>
<!DOCTYPE html>
<html>
<head>
<title>Liste des formations</title>
<%@include file="head/head.jsp" %>
<link rel="stylesheet" href="css/bd-wizard.css">
</head>
<body onload="initPage()">
	<div id="page">
		<jsp:include page="header/header.jsp"></jsp:include>
		<%@include file="nav/nav.jsp" %>
		
	
		
		<div id="content" style="margin-top:5em;margin-bottom:5em;">
			    <!-- la section formation  -->
         <section style="padding-left:5em;padding-right:5em;">
           <h2 class="section-heading">Sélectionnez la formation qui vous correspond </h2>
           <p> Ci-dessous les sessions prochaines sessions disponibles pour le domaine sélectionné.</p>
           <select id="selectFormation" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
			  <option selected value="0">Sélectionner le domaine </option>
	   			<c:forEach items="${requestScope.domaines }" var="domaine">
  					<option value="${domaine.id }">${domaine.nom }</option>
				</c:forEach>
			</select>
          </section>
          
    <p class="color black;"></br></br></br></br></br></br></br></p>
    <section class="ftco-section ftco-services ftco-no-pt" data-section="formations" >
    
      <div class="container">
     
        <div class="row-style services-section">

     <c:forEach items="${requestScope.themes }" var="theme">
     	
     <c:choose>
 
		         <c:when test = "${theme.domaine.id == 1}">
		            <c:set var = "image" value = "images/Informatique_Logo.png" />
		            <c:set var = "domaine" value = "INFORMATIQUE" />
		         </c:when>
		         
		         <c:when test = "${theme.domaine.id == 2}">
		            <c:set var = "image" value = "images/RH_logo.png" />
		            <c:set var = "domaine" value = "RESSOURCES HUMAINES" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 3}">
		            <c:set var = "image" value = "images/Reseaux_Logo.png" />
		            <c:set var = "domaine" value = "RÉSEAUX" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 4}">
		            <c:set var = "image" value = "images/Management_logo.png" />
		            <c:set var = "domaine" value = "MANAGEMENT" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 5}">
		            <c:set var = "image" value = "images/bureautique_logo.png" />
		            <c:set var = "domaine" value = "BUREAUTIQUE" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 6}">
		            <c:set var = "image" value = "images/Metiers_logo.png" />
		            <c:set var = "domaine" value = "MÉTIERS" />
		         </c:when>
		         
		         <c:otherwise>
		           	<c:set var = "image" value = "images/Informatique_Logo.png" />
		            <c:set var = "domaine" value = "INFORMATIQUE" />
		         </c:otherwise>
		      </c:choose>

      						<div class="col-md-4 col-lg-4 align-self-stretch ftco-animate card-section" attr-display=${theme.domaine.id}>
					          <div class="media block-6 services text-center d-block">
					         
					          
					          	
					            <img src="${image}" data section="">
					            <div class="mt-2 col-md-12">
					              <!--  ${formation.lieu } -->
					            </div>
					            <div class="media-body">
					              <h3 class="heading mb-3"> ${theme.nom }
								
					              </h3>
					              <p class="text-left">
						              <c:forEach items="${theme.sessions }" var="session">
						               • ${session.nom } </br>
						              </c:forEach>
									</p>			

					              <p><a href="inscriptionsession?theme=${theme.id}" class="btn btn-primary">S'inscrire</a></p>
					            </div>
					          </div>
					        </div>
						</c:forEach>


        </div>
      </div>

 

    </section>
		</div>
		
					
		<jsp:include page="footer/footer.jsp"></jsp:include>
	</div>
	<script>
$( document ).ready(function() {
   $(".active").removeClass("active");
   $("#formations").addClass("active");

   $('#selectFormation').on('change', function() {
	   if(this.value == "0")
	   {
		   $('.card-section[attr-display!='+this.value+']').removeClass("hide");
	   }
	   else 
		 {
		   $('.card-section[attr-display!='+this.value+']').addClass("hide");
		   $('.card-section[attr-display='+this.value+']').removeClass("hide");
		   //$('*[attr-display="'.this.value.'"]').css("background", "red");
		  }
	 
	 });
   
});
</script>
</body>
</html>
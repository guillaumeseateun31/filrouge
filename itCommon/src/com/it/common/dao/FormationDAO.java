package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Formation;

public class FormationDAO extends AbstractDAO<Formation> implements IFormationDAO {

	
	public FormationDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Formation> findAll() {
		List<Formation> formations = null;

		formations = em.createQuery("from Formation").getResultList();

		return formations;
	}


	@Override
	public Formation get(Integer id) {
		Formation formation = null;
		formation = em.find(Formation.class, id);
		return formation;
	}


}

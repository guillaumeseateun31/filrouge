/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Soustheme;
import com.it.common.bo.Theme;

/**
 * @author it
 *
 */
public interface IThemeFacade {

	List<Theme> findAll();
	
	List<Soustheme> getSousTheme(Integer id);

	Theme get(Integer id);

	Theme save(Theme theme);

	Theme update(Theme theme);

	void delete(Theme theme);

}

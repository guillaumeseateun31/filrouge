package com.it.rs.salle;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.it.common.facade.ISalleFacade;
import com.it.common.bo.Salle;

@Component
@Path("/salle")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class SalleService {

	private ISalleFacade salleFacade = null;

	public SalleService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("{id}")
	public Salle getSalle(@PathParam("id") Integer id) {
		Salle salle = salleFacade.get(id);
		return salle;
	}

	@GET
	@Path("/salles")
	public List<Salle> findAll() {
		List<Salle> salles = salleFacade.findAll();
		return salles;
	}

	@DELETE
	@Path("{id}")
	public String delete(@PathParam("id") Integer id) {
		Salle salle = new Salle(id, null, null, null, 0, null, null);
		salleFacade.delete(salle);
		return "ok";
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public String ajouter(Salle salle) {
		salleFacade.save(salle);
		return "ok";
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	public String update(Salle salle) {
		salleFacade.update(salle);
		return "ok";
	}

	public ISalleFacade getSalleFacade() {
		return salleFacade;
	}

	public void setSalleFacade(ISalleFacade salleFacade) {
		this.salleFacade = salleFacade;
	}



}
package com.it.rs.domaine;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.it.common.facade.IDomaineFacade;
import com.it.common.bo.Domaine;

@Component
@Path("/domaine")
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public class DomaineService {

	private IDomaineFacade domaineFacade = null;

	public DomaineService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@GET
	@Path("{id}")
	public Domaine getDomaine(@PathParam("id") Integer id) {
		Domaine domaine = domaineFacade.get(id);
		return domaine;
	}

	@GET
	@Path("/domaines")
	public List<Domaine> findAll() {
		List<Domaine> domaines = domaineFacade.findAll();
		return domaines;
	}

	@DELETE
	@Path("{id}")
	public String delete(@PathParam("id") Integer id) {
		Domaine domaine = new Domaine(id, null);
		domaineFacade.delete(domaine);
		return "ok";
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	public String ajouter(Domaine domaine) {
		domaineFacade.save(domaine);
		return "ok";
	}

	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	public String update(Domaine domaine) {
		domaineFacade.update(domaine);
		return "ok";
	}

	public IDomaineFacade getDomaineFacade() {
		return domaineFacade;
	}

	public void setDomaineFacade(IDomaineFacade domaineFacade) {
		this.domaineFacade = domaineFacade;
	}



}
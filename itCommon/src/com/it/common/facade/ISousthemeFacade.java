/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Soustheme;

/**
 * @author it
 *
 */
public interface ISousthemeFacade {

	List<Soustheme> findAll();

	Soustheme get(Integer id);

	Soustheme save(Soustheme soustheme);

	Soustheme update(Soustheme soustheme);

	void delete(Soustheme soustheme);

}

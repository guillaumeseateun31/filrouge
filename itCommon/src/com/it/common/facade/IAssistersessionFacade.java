/**
 * 
 */
package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Assistersession;

/**
 * @author it
 *
 */
public interface IAssistersessionFacade {

	List<Assistersession> findAll();

	Assistersession get(Integer id);
	
	List<Assistersession> getByUser(Integer id);

	Assistersession save(Assistersession assistersession);

	Assistersession update(Assistersession assistersession);

	void delete(Assistersession assistersession);

}

package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Theme implements Serializable {

	private static final long serialVersionUID = -1247371726916552927L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nom;
	private String description;
	@ManyToOne @JoinColumn(name="id_domaine", nullable=false)
    private Domaine domaine;
	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinTable(name = "decomposition_theme", joinColumns = @JoinColumn(name = "id_theme", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_soustheme", referencedColumnName = "id", updatable = true))
	private List<Soustheme> sousthemes = new ArrayList<Soustheme>();
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	@JoinTable(name = "session", joinColumns = @JoinColumn(name = "id_theme", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id", referencedColumnName = "id", updatable = true))
	private List<Session> sessions = new ArrayList<Session>();

	
	public Theme() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Theme(Integer id, String nom) {
		super();
		this.id = id;
		this.nom = nom;
	}



	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Domaine getDomaine() {
		return domaine;
	}

	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}

	public List<Soustheme> getSousthemes() {
		return sousthemes;
	}

	public void setSousthemes(List<Soustheme> sousthemes) {
		this.sousthemes = sousthemes;
	}

	public List<Session> getSessions() {
		return sessions;
	}

	public void setSessions(List<Session> sessions) {
		this.sessions = sessions;
	}
	
	

}

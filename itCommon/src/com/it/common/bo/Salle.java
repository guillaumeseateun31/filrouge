package com.it.common.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author it
 *
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Salle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2757971987182426556L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nomSite;
	private String nomSalle;
	private String ville;
	private float cout;
	private Integer nbrPlaces;
	private String codePostal;
	@OneToMany( targetEntity=Session.class, mappedBy="salle" )
    private List<Session> sessions = new ArrayList<>();
	
	public Salle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Salle(Integer id, String nomSite, String nomSalle, String ville, float cout, Integer nbrPlaces,
			String codePostal) {
		super();
		this.id = id;
		this.nomSite = nomSite;
		this.nomSalle = nomSalle;
		this.ville = ville;
		this.cout = cout;
		this.nbrPlaces = nbrPlaces;
		this.codePostal = codePostal;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomSite() {
		return nomSite;
	}
	public void setNomSite(String nomSite) {
		this.nomSite = nomSite;
	}
	public String getNomSalle() {
		return nomSalle;
	}
	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public float getCout() {
		return cout;
	}
	public void setCout(float cout) {
		this.cout = cout;
	}
	public Integer getNbrPlaces() {
		return nbrPlaces;
	}
	public void setNbrPlaces(Integer nbrPlaces) {
		this.nbrPlaces = nbrPlaces;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	


}

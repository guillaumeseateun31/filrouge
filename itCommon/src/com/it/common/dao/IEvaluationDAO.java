package com.it.common.dao;

import com.it.common.bo.Evaluation;

public interface IEvaluationDAO extends IDAO<Evaluation> {

}

package com.it.common.facade;

import java.util.List;

import com.it.common.bo.Typeresponsable;
import com.it.common.dao.ITyperesponsableDAO;

public class TyperesponsableFacade implements ITyperesponsableFacade {

	private ITyperesponsableDAO typeresponsableDAO = null;

	public TyperesponsableFacade() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Typeresponsable> findAll() {
		List<Typeresponsable> typeresponsables = null;

		// appel à la couche DAO
		typeresponsables = typeresponsableDAO.findAll();

		return typeresponsables;
	}

	public Typeresponsable save(Typeresponsable typeresponsable) {
		Typeresponsable typeresponsableSave = typeresponsableDAO.save(typeresponsable);
		return typeresponsableSave;
	}

	/**
	 * @return the typeresponsableDAO
	 */
	public ITyperesponsableDAO getTyperesponsableDAO() {
		return typeresponsableDAO;
	}

	/**
	 * @param typeresponsableDAO the typeresponsableDAO to set
	 */
	public void setTyperesponsableDAO(ITyperesponsableDAO typeresponsableDAO) {
		this.typeresponsableDAO = typeresponsableDAO;
	}

	@Override
	public Typeresponsable get(Integer id) {
		Typeresponsable typeresponsable = typeresponsableDAO.get(id);
		return typeresponsable;
	}

	@Override
	public Typeresponsable update(Typeresponsable typeresponsable) {
		Typeresponsable typeresponsableUpdate = typeresponsableDAO.update(typeresponsable);
		return typeresponsableUpdate;
	}

	@Override
	public void delete(Typeresponsable typeresponsable) {
		typeresponsableDAO.delete(typeresponsable);
	}

}

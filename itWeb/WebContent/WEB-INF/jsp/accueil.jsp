<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<title>Accueil It TRAINING</title>
<%@include file="head/head.jsp" %>
</head>
<body>
	<div id="page">
		<jsp:include page="header/header.jsp"></jsp:include>
		  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="index.html"><img src="images/Logo_IT.png" class="logo" alt="LOGO"> </a>
      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse"
        data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav nav ml-auto">
          <li class="nav-item" id="accueil"><a href="accueil" class="nav-link" data-nav-section="home" ><span>Accueil</span></a></li>
          <!--  <li class="nav-item" id="nous"><a href="#" class="nav-link" data-nav-section="about" ><span>Nous</span></a></li>-->
          <li class="nav-item" id="formations"><a href="formations" class="nav-link" data-nav-section="formations" ><span>Nos formations</span></a>
          </li>
         <!--  <li class="nav-item" id="agences"><a href="#" class="nav-link" data-nav-section="team" ><span>Agences</span></a></li>-->
          <li class="nav-item" id="contact"><a href="#" class="nav-link" data-nav-section="contact" ><span>Contact</span></a></li>
          <c:if test="${empty pageContext.request.userPrincipal }">
          	<!-- <li class="nav-item cta" ><button class="nav-link btn-it" data-toggle="modal" data-target="#loginModal" id="login">S'identifer</button></li>-->
          	<a id="btnLogin" href="sessions" class="nav-link btn-it">S'identifier</a>
          </c:if>
          <c:if test="${not empty pageContext.request.userPrincipal }">
          			<li class="nav-item" id="sessions"><a href="sessions" class="nav-link" data-nav-section="sessions" ><span>Sessions</span></a></li>
				 <li class="nav-item" id="ffg"><a href="#" class="nav-link" data-nav-section="contact" ><span style="color:grey;"><c:out value="${pageContext.request.userPrincipal.name }"></c:out></span></a></li>
				 <li class="nav-item" id="qsd"></li>
				 <li class="nav-item cta" ><a id="btnDeconnexion" href="deconnexion" class="nav-link btn-it">Deconnexion</a></li>
			</c:if>
        </ul>
      </div>
    </div>
  </nav>
  
    <!-- <section class="hero-wrap" style="background-image: url('images/machine-learning-et-big-data.jpg'); height: 170px;"
    data-section="home"> 
    -->
    <section class="" style="background-image: url('images/machine-learning-et-big-data.jpg');  background-size: cover;  height: 500px;">

      <div class="container">
        <div class="row-style no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-8 ftco-animate mt-5" data-scrollax=" properties: { translateY: '70%' }" style="margin-bottom: 10em;">

            <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">Découvrez nos formations les plus populaires</h1>
            
          </div>
        </div>
      </div>
    
  </section>
  
		<div id="content">
		
			<section class="ftco-section ftco-services ftco-no-pt" style="">

			    <div class="container">
			      <div class="row-style services-section">
			      
			      <c:set var="count" value="0" scope="page" />
      					  <c:forEach items="${requestScope.themes }" var="theme">
     	<c:if test = "${count < 6}">
         
     	
     <c:choose>
 
		         <c:when test = "${theme.domaine.id == 1}">
		            <c:set var = "image" value = "images/Informatique_Logo.png" />
		            <c:set var = "domaine" value = "INFORMATIQUE" />
		         </c:when>
		         
		         <c:when test = "${theme.domaine.id == 2}">
		            <c:set var = "image" value = "images/RH_logo.png" />
		            <c:set var = "domaine" value = "RESSOURCES HUMAINES" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 3}">
		            <c:set var = "image" value = "images/Reseaux_Logo.png" />
		            <c:set var = "domaine" value = "RÉSEAUX" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 4}">
		            <c:set var = "image" value = "images/Management_logo.png" />
		            <c:set var = "domaine" value = "MANAGEMENT" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 5}">
		            <c:set var = "image" value = "images/bureautique_logo.png" />
		            <c:set var = "domaine" value = "BUREAUTIQUE" />
		         </c:when>
		         <c:when test = "${theme.domaine.id == 6}">
		            <c:set var = "image" value = "images/Metiers_logo.png" />
		            <c:set var = "domaine" value = "MÉTIERS" />
		         </c:when>
		         
		         <c:otherwise>
		           	<c:set var = "image" value = "images/Informatique_Logo.png" />
		            <c:set var = "domaine" value = "INFORMATIQUE" />
		         </c:otherwise>
		      </c:choose>

      						<div class="col-md-4 col-lg-4 align-self-stretch ftco-animate card-section" attr-display=${theme.domaine.id}>
					          <div class="media block-6 services text-center d-block">
					         
					          
					          	
					            <img src="${image}" data section="">
					            <div class="mt-2 col-md-12">
					              <!--  ${formation.lieu } -->
					            </div>
					            <div class="media-body">
					              <h3 class="heading mb-3"> ${theme.nom }
								
					              </h3>
					              <p class="text-left">
						              <c:forEach items="${theme.sessions }" var="session">
						               • ${session.nom } </br>
						              </c:forEach>
									</p>			

					              <p><a href="inscriptionsession?theme=${theme.id}" class="btn btn-primary">S'inscrire</a></p>
					            </div>
					          </div>
					        </div>
					    <c:set var="count" value="${count + 1}" scope="page"/>
					     </c:if>
						</c:forEach>

			      </div>
			    </div>
			  </section>
			  
			  <section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="section-counter" >
		    	<div class="container">
		    		<div class="row-style d-flex">
		    		<div class="col-md-12 col-lg-12 pl-lg-12 py-5">
    				<div class="row-style justify-content-start pb-3">
		          <div class="col-md-12 heading-section ftco-animate">
		          	<span class="subheading">Quelques chiffres</span>
		            <h2 class="mb-4">Année 2020</h2>
		            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
		            <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
		            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
		          </div>
		        </div>
		    		<div class="row-style">
		          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
		            <div class="block-18 text-center p-4 mb-4 align-self-stretch d-flex">
		              <div class="text">
		                <strong class="number" data-number="6">0</strong>
		                <span>Nombre d'agences</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
		            <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
		              <div class="text">
		                <strong class="number" data-number="258">0</strong>
		                <span>Formations terminées</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
		            <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
		              <div class="text">
		                <strong class="number" data-number="90">0</strong>%
		                <span>Taux de réinsertion</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
		            <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
		              <div class="text">
		                <strong class="number" data-number="4500">0</strong>
		                <span>Moyenne des salaires</span>
		              </div>
		            </div>
		          </div>
		        </div>
	        	</div>
	       	 </div>
	        </div>
	        </section>
		</div>
		

		
		<jsp:include page="footer/footer.jsp"></jsp:include>
	</div>
<script>
$( document ).ready(function() {
   $(".active").removeClass("active");
   $("#accueil").addClass("active");
});
</script>
</body>
</html>
package com.it.common.bo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Client implements Serializable {

	private static final long serialVersionUID = 6314574962871994541L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nomSociete;
	private String siret;
	@ManyToOne @JoinColumn(name="id_utilisateur", nullable=false)
    private Utilisateur utilisateur;
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Client(Integer id, String nomSociete, String siret) {
		super();
		this.id = id;
		this.nomSociete = nomSociete;
		this.siret = siret;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomSociete() {
		return nomSociete;
	}

	public void setNomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
	}

	public String getSiret() {
		return siret;
	}

	public void setSiret(String siret) {
		this.siret = siret;
	}






	

}

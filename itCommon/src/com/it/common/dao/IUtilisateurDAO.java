package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Session;
import com.it.common.bo.Utilisateur;

public interface IUtilisateurDAO extends IDAO<Utilisateur> {
	Utilisateur getByEmail(String email);
}

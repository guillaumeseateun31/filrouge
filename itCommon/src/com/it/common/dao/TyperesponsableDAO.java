package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Typeresponsable;
import com.it.common.bo.Stagiaire;

public class TyperesponsableDAO extends AbstractDAO<Typeresponsable> implements ITyperesponsableDAO {

	
	public TyperesponsableDAO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	@Override
	public List<Typeresponsable> findAll() {
		List<Typeresponsable> Typeresponsables = null;

		Typeresponsables = em.createQuery("from Typeresponsable").getResultList();

		return Typeresponsables;
	}
	
	@Override
	public Typeresponsable save(Typeresponsable typeresponsable) {

		return super.save(typeresponsable);
	}


	@Override
	public Typeresponsable get(Integer id) {
		Typeresponsable Typeresponsable = null;
		Typeresponsable = em.find(Typeresponsable.class, id);
		return Typeresponsable;
	}


}

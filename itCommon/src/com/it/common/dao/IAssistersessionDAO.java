package com.it.common.dao;

import java.util.List;

import com.it.common.bo.Assistersession;

public interface IAssistersessionDAO extends IDAO<Assistersession> {
	List<Assistersession> getByUser(Integer id);
}
